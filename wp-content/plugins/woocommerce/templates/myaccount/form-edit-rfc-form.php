<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
$user = wp_get_current_user();
$user_id = get_current_user_id();

?>

<form class="woocommerce-EditAccountFor" action="" method="post" >
    <div class="form-wrapper">
        <legend><?php esc_html_e( 'Agregar RFC:', 'woocommerce' ); ?></legend>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container">
            <label for="account_first_name"><?php esc_html_e( 'Nombre o Razón social', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input placeholder="Nombre (s)" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_name" id="account_rfc_name" autocomplete="" value="<?php echo esc_attr( $user->account_rfc_name ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container">
            <label for="account_rfc"><?php esc_html_e( 'RFC', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input data-validate="rfcNumber" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc" id="account_rfc" autocomplete="rfc" value="<?php echo esc_attr( $user->account_rfc ); ?>" />
        </p>
        <div class="address">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container large-size">
                <label for="account_rfc_address"><?php esc_html_e( 'Calle y número', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_address" id="account_rfc_address" autocomplete="" value="" />
            </p>
            <p class="woocommerce-form-row form-row input-container small-size">
                <input placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_ext" id="account_rfc_no_ext" autocomplete="" value="" />
            </p>
            <p class="woocommerce-form-row form-row input-container small-size">
                <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_int" id="account_rfc_no_int" autocomplete="" value="" />
            </p>
        </div>
        <div class="address">
            <p class="woocommerce-form-row form-row input-container large-size">
                <input placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_colony" id="account_rfc_colony" autocomplete="" value="" />
            </p>
            <p class="woocommerce-form-row form-row input-container medium-size">
                <input placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_cp" id="account_rfc_cp" autocomplete="" value="" />
            </p>
        </div>
        <div class="address">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container large-size">
                <label for="account_rfc_address"><?php esc_html_e( 'Ciudad y estado', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
                <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_city" id="account_rfc_address" autocomplete="" value="" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container medium-size">
                <input placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_state" id="account_rfc_no_ext" autocomplete="" value="" />
            </p>
        </div>
    </div>
    <div class="server-response">
        <!--//Enviando-->
        <div class="icon">
            <div class="text">
                <div class="info">
                    <img src="wp-content/themes/inksumos/assets/images/saving_data.svg" alt="Saving Data" class="img-fluid">
                </div>
                <div class="title"></div>
            </div>
        </div>
        <!--//Success-->
        <div class="icon">
            <div class="text">
                <div class="info"><img src="wp-content/themes/inksumos/assets/images/saved_data.svg" alt="Data Saved" class="img-fluid"></div>
                <div class="title"></div>
            </div>
        </div>
        <!--//Error-->
        <div class="icon">
            <div class="text">
                <div class="info"></div>
                <div class="title">
                    Ocurrió un Error. Inténtelo más tarde.
                </div>
            </div>
        </div>
    </div>
    <p>
        <button type="submit" class="woocommerce-Button button" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Guardar >', 'woocommerce' ); ?></button>
    </p>
</form>
