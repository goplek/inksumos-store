<?php
//$postPrivacy = get_post(3);
//$privacy = $postPrivacy->guid;
$post = get_post(3);
$privacy = get_permalink();

$post = get_post(171);
$terms = get_permalink();
/*$postTerms = get_post(171);
$terms = $postTerms->guid;*/

$post = get_post(174);
$ask = get_permalink();
/*
$postAsk = get_post(174);
$ask = $postAsk->guid;
 */

?>
<!-- Footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-sm-12 col-md-6 order-md-2">
                <div class="links">
                    <div class="logo">
                        <img src="<?= get_image_uri('icon-logo.svg') ?>" alt="Inksumos">
                    </div>
                    <ul>
                        <li><a href="<?= $privacy ?>">Aviso de privacidad</a></li>
                        <li><a href="<?= $terms ?>">Términos y condiciones</a></li>
                        <li><a href="<?= $ask ?>">Preguntas frecuentes</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-9 col-md-3 order-md-1">
                <div class="rights">
                    <div>© 2018 inksumos.com</div>
                    <div>Todos los derechos reservados.</div>
                </div>
            </div>
            <div class="col-3 col-md-3 order-md-3">
                <a href="http://goplek.com" class="logo-goplek" target="_blank">
                    <img src="<?= get_image_uri('icon-goplek.svg') ?>" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>
</div> <!-- Outer Wrapper -->

<!--   Material Kit Core JS Files   -->
<script src="<?= JS_PATH ?>vendor/materialkit/core/jquery.min.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>vendor/materialkit/core/popper.min.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>vendor/materialkit/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>vendor/materialkit/plugins/moment.min.js"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="<?= JS_PATH ?>vendor/materialkit/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?= JS_PATH ?>vendor/materialkit/plugins/nouislider.min.js" type="text/javascript"></script>
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="<?= JS_PATH ?>vendor/materialkit/material-kit.min.js" type="text/javascript"></script>
<!-- Javascript Files -->
<script src="<?= JS_PATH ?>vendor/jquery.mobile.custom.min.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>pl-carrousel.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>pl-slider.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>pl-contact-form.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>pl-modal.js" type="text/javascript"></script>
<script src="<?= JS_PATH ?>pl-range-slide.js" type="text/javascript"></script>
<!-- Control Center for Javascript File -->
<script src="<?= JS_PATH ?>scripts.js" type="text/javascript"></script>
</body>
</html>