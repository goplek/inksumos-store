<section class="products-cover">
    <div class="holder">
        <div class="wrapper-products-cover">
            <div class="products-cover-text">
                <b>Comprar impresoras y cartuchos</b> nunca fue tan fácil.
            </div>
            <div class="products-cover-image">
                <img src="<?= get_image_uri('printer.png') ?>" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>

