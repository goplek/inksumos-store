<?php
/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 3/21/19
 * Time: 1:12 PM
 */
?>
<section class="cover-services">
    <div class="holder">
        <div class="wrapper-services-text">
            <div class="inner-services-text">
                <b>Mientras te enfocas en tu negocio,</b> nosotros nos encargamos de tus impresiones.
            </div>
        </div>
    </div>
</section>

<section class="message-services">
    <div class="holder">
        <div class="wrapper-content-message">
            Cubrimos tus necesidades de impresión al proveerte de toners, servicios y el equipo que tu empresa necesita
            por una cuota mensual muy accesible.
        </div>
    </div>
</section>

<section class="content-services">
    <div class="holder">

        <div class="subtitle">
            Ventajas
        </div>

        <div class="content-advantages">

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('drop.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        No pagas toner*
                    </div>
                    <div class="description-advantage">
                        Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                    </div>
                    <div class="note">
                        *Impresiones limitadas
                    </div>
                </div>
            </div>

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('printer.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        Te ahorras el equipo
                    </div>
                    <div class="description-advantage">
                        Recorta costos por medio del arrendamiento.
                    </div>
                    <div class="note">
                    </div>
                </div>
            </div>

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('contract.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        Contrato de 6 meses
                    </div>
                    <div class="description-advantage">
                        No te obligamos a plazos forzosos por periodos de tiempo muy largos.
                    </div>
                    <div class="note">
                    </div>
                </div>
            </div>

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('fill.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        10% del costo de toner
                    </div>
                    <div class="description-advantage">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.
                    </div>
                    <div class="note">
                    </div>
                </div>
            </div>

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('replacement.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        Reemplazo de equipo
                    </div>
                    <div class="description-advantage">
                        Te ofrecemos equipo sustituto a tu disposición (en caso de descomposturas)
                    </div>
                    <div class="note">
                    </div>
                </div>
            </div>

            <div class="advantage">
                <div class="inner-advantage">
                    <div class="image-advantage">
                        <img src="<?= get_image_uri('maintenance.svg') ?>" alt="" class="img-fluid">
                    </div>
                    <div class="advantage-title">
                        Mantenimiento sin costo
                    </div>
                    <div class="description-advantage">
                        Todas las reparaciones y refacciones van por nuestra cuenta.
                    </div>
                    <div class="note">
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<section class="block contact-inner-services">
    <div class="holder">

        <div class="wrapper-btn-information-services">
            <div class="inner-btn-information-services">
              <b>Envíanos un mensaje</b> para solicitar más información.
            </div>
        </div>

        <div class="wrapper-logo-services">
            <div class="inner-logo-services">
                <img src="<?= get_image_uri('logo-01.svg') ?>" alt="" class="img-fluid">
            </div>
        </div>
        <div class="wrapper-contact-services">
            <form>
                <div class="status-message d-none"></div>
                <div class="form-row">
                    <div class="wrapper-content-form">
                        <div class="col input-services">
                            <div class="wrapper-name-services">
                                <input autocomplete="off" class="form-control" data-clue="Nombre*"
                                       data-placeholder="Nombre*" data-validate="notEmpty" name="Nombre*"
                                       placeholder="Nombre*" type="text">
                            </div>
                            <div class="wrapper-email-services">
                                <input autocomplete="off" class="form-control" data-clue="Correo*"
                                       data-placeholder="Correo*" data-validate="email" name="Email"
                                       placeholder="Correo*" type="text">
                            </div>
                        </div>

                        <div class="col textarea-services">
                        <textarea class="form-control" data-validate="notEmpty" data-clue="Mensaje*"
                                  name="Mensaje" placeholder="Mensaje*" data-placeholder="Mensaje*"></textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-rose btn-raised btn-round btn-meet btn-submit-services">
                        Enviar
                    </button>

                </div>
            </form>

        </div>
    </div>
</section>

