<?php
/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 3/13/19
 * Time: 11:33 AM
 */

$orderby = 'name';
$order = 'asc';
$hide_empty = false;
$cat_args = array(
    'orderby' => $orderby,
    'order' => $order,
    'hide_empty' => $hide_empty,
);
$product_categories = get_terms('product_cat', $cat_args);
?>
<section class="inner-nav">
    <div class="holder">
        <div class="wrapper-inner-nav">
            <div class="btns-category">
                <?php
                foreach ($product_categories as $category) {
                    if ($category->slug === 'impresiones' || $category->slug === 'cartuchos') {
                        ?>
                        <div class="category-product" data-category="<?= $category->slug ?>"
                             data-selected="<?= $category->term_id ?>">
                            <span><?= $category->name ?></span>
                        </div>
                        <?php
                    }
                }
                ?>
                <div class="categorySelect d-none"></div>
            </div>
            <div class="view-nav-inner">
                <div class="text">
                    Ver:
                </div>
                <div class="view-nav-grid type-view active">
                </div>
                <div class="view-nav-list type-view">
                </div>
            </div>
        </div>
    </div>
</section>
