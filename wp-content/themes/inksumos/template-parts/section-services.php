<?php
global $post;
$args = array('post_type' => 'product', 'product_cat' => 'promociones', 'orderby' => 'title', 'order' => 'DESC', 'posts_per_page' => '4',);
$loop = new WP_Query($args);
$post_to_show = 8;
?>
<section class="block services" id="servicios">
    <div class="holder">
        <div class="container-fluid">
            <div class="wrapper-services">
                <div class="inner-wrapper-services">
                    <div class="card services bg-dark text-white">
                        <div class="wrapper-card-header">
                            <div class="card-header">
                                <h4 class="card-title">Servicios</h4>
                            </div>
                        </div>
                        <img class="card-img-top" src="<?= get_image_uri('servicios.jpg') ?>" alt="Card image">
                        <div class="card-img-overlay">
                            <div class="wrapper-card-img-overlay">
                                <p class="card-text title-service">Impresión</p>
                                <p class="card-text phrase">Mientras te enfocas en tu negocio, nosotros nos encargamos
                                    de tus impresiones.</p>
                            </div>
                        </div>
                        <div class="wrapper-btn-meet">
                            <?php
                            $post = get_post(93);
                            $guid_page = get_permalink();
                            ?>
                            <a href="<?= $guid_page ?>" class="btn btn-rose btn-raised btn-round btn-meet">
                                <span>Conoce más</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper-promotions">
                <div class="title-promotions">
                    <div class="title-text-promotions">
                        Artículos en promoción
                    </div>
                </div>
                <div class="inner-promotions-carousel hide-moment">
                    <div class="inner-carrousel">
                        <div class="util-carrousel">
                            <!---->
                            <?php
                            $count_home = 1;
                            while ($loop->have_posts()) : $loop->the_post();
                                global $product;

                                $sku_home = get_post_meta(get_the_ID(), '_sku', true);
                                $stock_home = $product->get_stock_quantity(); //quantity in stock
                                $allImagesViewProduct_home = $product->get_gallery_image_ids();

                                $terms = get_the_terms($post->ID, 'product_cat');

                                $id_product = $post->ID;

                                foreach ($terms as $term) {

                                    $product_cat_id = $term->term_id;

                                    if (($count_home % 2) !== 0) {

                                        ?>
                                        <div class="thumb">
                                            <a class="inner-thumb product-products1" href="javascript:void(0)"
                                               data-category="<?= $product_cat_id ?>" data-id="<?= $id_product ?>">
                                                <div class="card card-profile ml-auto mr-auto" style="max-width: 360px">
                                                    <div class="card-header card-header-image">
                                                        <div class="wrapper-img-carousel image-product-products">
                                                            <img src="<?= get_the_post_thumbnail($loop->post->ID) ?>">
                                                        </div>
                                                    </div>
                                                    <div class="card-body card-body-text">
                                                        <h4 class="card-title card-body-title"><?= esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID) ?></h4>
                                                        <p class="card-text card-body-sku">Sku <?= $sku_home ?></p>
                                                        <div class="view-product-gallery d-none">
                                                            <?php
                                                            foreach ($allImagesViewProduct_home as $imageViewProduct_home) {
                                                                $image_link_home = wp_get_attachment_url($imageViewProduct_home);

                                                                ?>
                                                                <img src="<?= $image_link_home ?>"

                                                                     class="img-fluid">
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="description-complete d-none">
                                                            <?= $product->get_description() ?>
                                                        </div>
                                                        <div class="short-description">
                                                            <?php the_excerpt() ?>
                                                        </div>
                                                        <div class="wrapper-about-product">
                                                            <p class="card-text about-product-price"><?= $product->get_price_html() ?></p>
                                                            <?php
                                                            $post = get_post(6);
                                                            $guid_cart_page = get_permalink();
                                                            ?>
                                                            <div class="inner-about-product"
                                                                 data-link="<?= $guid_cart_page . '&add-to-cart=' . $id_product ?>">
                                                                <p class="card-text about-product-available">
                                                                    Disponible</p>
                                                                <div class="wrapper-car-price">
                                                                    <img class="img"
                                                                         src="<?= get_image_uri('shopping-cart.svg') ?>"
                                                                         alt="">
                                                                    <img class="white-cart d-none"
                                                                         src="<?= get_image_uri('white-cart.svg') ?>"
                                                                         alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <?php
                                    }
                                    $count_home++;
                                }
                            endwhile;
                            ?>
                            <!------>
                        </div>
                    </div>
                    <?php
                    $post = get_post(96);
                    $guid_promotions_page = get_permalink();
                    //$guid_promotions_page = $post->guid;
                    ?>
                    <a href="<?= $guid_promotions_page ?>" class="btn btn-rose btn-raised btn-round see-more-products"
                       category="Promociones">
                        <span>Ver más</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>