<?php
$post = get_post(157);
$content_section = $post->post_content;
$arrayImagenesSrc = array();
$arrayTextContent = array();
$patternImg = '/<img (.+?)>/';

if (preg_match_all($patternImg, $content_section, $matches)) {
    foreach ($matches[1] as $match) {
        foreach (wp_kses_hair($match, array('http')) as $attr)
            $img[$attr['name']] = $attr['value'];
        $img_children_src = $img['src'];
        array_push($arrayImagenesSrc, $img_children_src);
    }
}

?>
<!-- Block Business -->
<section class="block business" id="business">
    <div class="holder">
        <div class="container-fluid">
            <div class="header">
                <div class="main-title">
                    <h2 class="title">
                        <span>
                            <img src="<?= $arrayImagenesSrc[0] ?>" alt="" class="img-fluid">
                        </span>
                        <?= $post->post_title ?>
                    </h2>
                </div>
            </div>
            <div class="content">
                <div class="content-title">
                    <a href="javascript:void(0)">
                        <div>¿Tu empresa tiene sucursales? Inksumos te soluciona la administación.</div>
                        <div class="more">Saber más</div>
                    </a>
                </div>
                <div class="content-info">
                    <div class="content-wrapper">
                        <div class="intro">
                            Dentro de Inksumos, el responsable
                            de compras puede cotizar, distribuir
                            y supervisar los pedidos a todas sus
                            sucursales a nivel Nacional.
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="img">
                                    <img src="<?= $arrayImagenesSrc[1] ?>" alt=""
                                         class="img-fluid">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="advantages">
                                    <div class="header">
                                        <div class="main-title">
                                            <h2 class="title">Ventajas</h2>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Control con compras centralizadas.</li>
                                        <li>Ahorro de tiempo y dinero en envíos.</li>
                                        <li>Eficiencia al consolidar con un proveedor.</li>
                                        <li>Precios preferenciales.</li>
                                        <li>Crédito.</li>
                                        <li>Atención personalizada.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Business -->
<script type="text/javascript">
    function setupAccordion() {
        let elementP = document.querySelector('.block.business .content-title');
        let element = document.querySelector('.block.business .content-title a');
        let ewrapper = document.querySelector('.block.business .content-wrapper');
        let econtainer = document.querySelector('.block.business .content-info');
        let originalSize = ewrapper.offsetHeight;
        setTimeout(function () {
            element.click();
        }, 5);
        element.addEventListener('click', function () {
            if (!(econtainer.classList.contains('active'))) {
                econtainer.classList.add('active');
                elementP.classList.add('active');
                econtainer.style.height = originalSize.toString() + 'px';
            }
            else {
                econtainer.classList.remove('active');
                elementP.classList.remove('active');
                econtainer.style.removeProperty('height');
            }
        });
    }

    document.addEventListener('DOMContentLoaded', function () {
        setupAccordion();
    }, false);
</script>