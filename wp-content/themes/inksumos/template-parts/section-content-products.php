<?php
/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 3/14/19
 * Time: 11:07 AM
 */

$args = array('post_type' => 'product', 'product_cat' => 'promociones', 'orderby' => 'title', 'order' => 'DESC');
$loop = new WP_Query($args);
$post_to_show = 8;
$terms = get_terms();

?>
<section class="block content-products-view">
    <div class="holder">
        <div class="wrapper-content-products">
            <div class="main-to-products">
                <div class="wrapper-search">
                    <form method="get" action="/" class="form-inline ml-auto search-products"
                           onsubmit="if (Page.validateBtnSearch()) {event.preventDefault();}">
                        <div class="form-group no-border">
                            <input type="text" class="form-control" name="s" placeholder="Buscar"
                                   onclick="if (Page.validateBtnSearch()) {event.preventDefault();}">
                        </div>
                        <button type="submit" class="btn btn-white btn-just-icon btn-round btn-sbmt-search">
                            <i class="material-icons">search</i>
                        </button>
                    </form>
                </div>
                <div class="wrapper-content-filters initializeSection">
                    <div class="title-main-products">
                        <span></span>
                    </div>
                    <?php
                    $attribute_taxonomies = wc_get_attribute_taxonomies();
                    foreach ($attribute_taxonomies as $tax) {
                        $tax_name_clean = ucfirst(str_replace("-", " ", $tax->attribute_name));
                        ?>
                        <!---------->
                        <div class="filters" data-selected="<?= $tax->attribute_id ?>">
                            <div class="inner-filter">
                                <div class="title-filter"><?= $tax_name_clean ?></div>
                                <div class="arrow-symbol"></div>
                            </div>
                            <div class="content-filter">
                                <div class="inner-container">
                                    <div class="content-features">
                                        <?php
                                        $attribute_name = get_terms(wc_attribute_taxonomy_name($tax->attribute_name));
                                        foreach ($attribute_name as $name) {
                                            ?>
                                            <label for="<?= slugify($name->name) ?>" class="container">
                                                <input id="<?= slugify($name->name) ?>" name="<?= slugify($name->name) ?>" type="checkbox" onclick="event.stopPropagation();">
                                                <?= $name->name ?>
                                                <span class="checkmark">
                                                </span>
                                            </label>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="body-content-products">
                <!---->

                <?php

                $count = 1;
                $extraClass = '';

                while ($loop->have_posts()) : $loop->the_post();
                    global $product;
                    $stock = $product->get_stock_quantity(); //quantity in stock

                    if ($count > $post_to_show) {
                        $extraClass = 'd-none';
                    }

                    $terms = get_the_terms($post->ID, 'product_cat');

                    /**
                     * Get all attributes of product
                     */

                   /*
                   $array_names = array();
                   $attributes = $product->get_attributes();

                    foreach ($attributes as $attribute) {
                        $terms_attributes = $attribute->get_terms();
                        foreach ($terms_attributes as $attributes) {
                            array_push($array_names, $attributes->name);
                        }
                    }
                    $values_attributes = implode(',', $array_names);*/

                    foreach ($terms as $term) {
                        if (($count % 2) !== 0) {
                            ?>
                            <?php
                            include "content/product-component.php";
                            ?>
                            <?php
                        }
                        $count++;
                    }
                endwhile;
                ?>

                <!---->
                <div class="wrapper-more-inner-products">
                    <a href="javascript:void(0)" class="btn btn-rose btn-raised btn-round see-more-inner-products">
                        <span>Ver más</span>
                    </a>
                </div>
                <!---->
            </div>
        </div>
    </div>
</section>
