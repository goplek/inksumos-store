<!-- Block Contact -->
<section class="block contact" id="contacto">
    <div class="holder">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-4 mobile-background">
                    <div class="header">
                        <h2 class="title">
                            Contacto
                            <span class="line-decoration"><span></span></span>
                        </h2>
                        <div>¿Tienes alguna duda o sugerencia?</div>

                        <div class="fake-button">Solicita tu cotización</div>
                    </div>
                    <div class="content">
                        <ul class="links">
                            <li><a href="#"><i><img src="<?= get_image_uri('icon-location.svg') ?>" alt="Ubicación"></i> Lorem ipsum dolor sit amet.</a></li>
                            <li><a href="#"><i><img src="<?= get_image_uri('icon-phone.svg') ?>" alt="Teléfono"></i> Lorem ipsum dolor sit amet.</a></li>
                            <li><a href="#"><i><img src="<?= get_image_uri('icon-whatsapp.svg') ?>" alt="Whatsapp"></i> 2225-88-8932</a></li>
                            <li><a href="#"><i><img src="<?= get_image_uri('icon-skype.svg') ?>" alt="Skype"></i> Lorem ipsum dolor sit amet.</a></li>
                            <li><a href="#"><i><img src="<?= get_image_uri('icon-facebook.svg') ?>" alt="Facebook"></i> Lorem ipsum dolor sit amet.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 offset-md-2">
                    <div class="card form-card">
                        <div class="card-header card-header-rose">
                            <h4 class="card-title text-center text-uppercase">Envíanos tu solicitud</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-text">Escribe tus datos para contactarnos:</div>
                            <form>
                                <div class="form-group has-rose bmd-form-group">
                                    <label class="bmd-label-floating">Escribe tu nombre*</label>
                                    <input name="Nombre" type="text" class="form-control">
                                </div>
                                <div class="form-group has-rose bmd-form-group">
                                    <label class="bmd-label-floating">Escribe tu correo*</label>
                                    <input name="Correo" type="text" class="form-control">
                                </div>
                                <div class="form-group has-rose bmd-form-group">
                                    <label class="bmd-label-floating">Escribe un número de diez dígitos*</label>
                                    <input name="Teléfono" type="text" class="form-control">
                                </div>
                                <div class="form-group has-rose bmd-form-group">
                                    <label class="bmd-label-floating">Escribe tu mensaje*</label>
                                    <textarea name="Mensaje" class="form-control"></textarea>
                                </div>
                                <button class="btn btn-primary btn-rose btn-round center" type="submit">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Contact -->