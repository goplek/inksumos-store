<!-- Block  -->
<section class="block cover page" id="portada">
    <div class="slider-content">
        <div class="slide">
            <div class="content">
                <div class="image-cover"></div>
                <div class="container">
                    <b>Mientras te enfocas en tu negocio,</b> nosotros nos encargamos de tus impresiones.
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /. -->