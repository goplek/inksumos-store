<?php
global $product;
$array_names = array();
$attributes = $product->get_attributes();

foreach ($attributes as $attribute) {
    $terms_attributes = $attribute->get_terms();
    foreach ($terms_attributes as $attributes) {
        array_push($array_names, $attributes->name);
    }
}

$values_attributes = implode(',', $array_names);
$product_tags = explode(",", $values_attributes);
foreach ($product_tags as $index => $tag) {
    $product_tags[$index] = slugify($tag);
}
?>
<a class="product-products1 <?= $extraClass ?> product-products-component <?= implode(" ", $product_tags) ?>"
   data-category="<?= isset($term->term_id) ? $term->term_id : '' ?>"
   href="javascript:void(0)" data-attributes="<?= isset($values_attributes) ? $values_attributes : "" ?>"
   data-id="<?= $post->ID ?>" data-slug="<?= isset($product) ? $product->get_slug() : "" ?>">
    <div class="card card-profile ml-auto mr-auto">
        <div class="card-header card-header-image">
            <div class="image-product-products">
                <img src="<?= get_the_post_thumbnail($loop->post->ID) ?>">
            </div>
        </div>
        <div class="card-body card-body-text">
            <div class="list-content-description">
                <!---<h4 class="card-title card-body-title"><?= esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID) ?></h4>-->
                <h4 class="card-title card-body-title"><?= isset($product) ? $product->get_name() : ''; ?></h4>
                <p class="card-text card-body-sku">
                    Sku <?= isset($product) ? $product->get_sku() : ''; ?>
                </p>
            </div>
            <div class="view-product-gallery d-none">
                <?php
                $allImagesViewProduct = $product->get_gallery_image_ids();
                foreach ($allImagesViewProduct as $imageViewProduct) {
                    $image_link = wp_get_attachment_url($imageViewProduct);
                    ?>
                    <img src="<?= $image_link ?>" alt="" class="img-fluid">
                    <?php
                }
                ?>
            </div>
            <div class="description-complete d-none">
                <?= $product->get_description() ?>
            </div>
            <div class="short-description">
                <?php the_excerpt() ?>
            </div>
            <div class="wrapper-about-product">
                <p class="card-text about-product-price"><?= $product->get_price_html() ?></p>
                <?php
                $post = get_post(6);
                //$guid_cart_page = $post->guid;
                $guid_cart_page = $guid_page = get_permalink();
                ?>
                <div class="inner-about-product"
                     data-link="<?= $guid_cart_page . '&add-to-cart=' . $product->get_id(); ?>">
                    <p class="card-text about-product-available ">Disponible</p>
                    <div class="wrapper-car-price">
                        <img class="img" src="<?= get_image_uri('shopping-cart.svg') ?>"
                             alt="">
                        <img class="white-cart d-none"
                             src="<?= get_image_uri('white-cart.svg') ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>
