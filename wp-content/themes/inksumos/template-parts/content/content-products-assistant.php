<?php
/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 6/5/19
 * Time: 1:25 PM
 */

$args = array('post_type' => 'product', 'product_cat' => 'impresiones', 'orderby' => 'title', 'order' => 'DESC');
$loop = new WP_Query($args);
$terms = get_terms();
?>
<section class="assistant-result d-none">
    <div class="holder">
        <div class="title-result-assistant">Estos son los equipos que te recomendamos:</div>
        <div class="content-products-result-assistant">
            <?php
            $count = 1;
            $extraClass = '';
            while ($loop->have_posts()):$loop->the_post();
                global $product;

                $terms = get_the_terms($post->ID, 'product_cat');

                $array_names = array();
                $attributes = $product->get_attributes();

                foreach ($attributes as $attribute) {
                    $terms_attributes = $attribute->get_terms();
                    foreach ($terms_attributes as $attributes) {
                        array_push($array_names, $attributes->name);
                    }
                }

                $values_attributes = implode(',', $array_names);

                foreach ($terms as $term) {
                    if (($count % 2) !== 0) {
                        ?>
                        <?php
                        include "product-component.php";
                        ?>
                        <?php
                    }
                    $count++;
                }
            endwhile;
            ?>
        </div>
        <div class="return-initialize-assistant">
            <div class="inner-return-assistant">
                Iniciar de nuevo
            </div>
        </div>
    </div>
</section>