<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

$args = array('post_type' => 'product', 'product_cat' => 'promociones', 'orderby' => 'title', 'order' => 'DESC');
$loop = new WP_Query($args);
$post_to_show = 8;
$terms = get_terms();

?>
<a class="product-products  <?= $extraClass ?>" data-category="<?= $term->term_id ?>"
   href="javascript:void(0)" data-attributes="<?= $values_attributes ?>"
   data-id="<?= $post->ID ?>">
    <div class="card card-profile ml-auto mr-auto">
        <div class="card-header card-header-image">
            <div class="image-product-products">
                <img src="<?= get_the_post_thumbnail($loop->post->ID) ?>">
            </div>
        </div>
        <div class="card-body card-body-text">
            <div class="list-content-description">
                <h4 class="card-title card-body-title"><?= esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID) ?></h4>
                <p class="card-text card-body-sku">Sku <?= get_post_meta(get_the_ID(), '_sku', true); ?></p>
            </div>
            <div class="view-product-gallery d-none">
                <?php
                $allImagesViewProduct = $product->get_gallery_image_ids();
                foreach ($allImagesViewProduct as $imageViewProduct) {
                    $image_link = wp_get_attachment_url($imageViewProduct);
                    ?>
                    <img src="<?= $image_link ?>" alt="" class="img-fluid">
                    <?php
                }
                ?>
            </div>
            <div class="description-complete d-none">
                <?= $product->get_description() ?>
            </div>
            <div class="short-description">
                <?php the_excerpt() ?>
            </div>

            <div class="wrapper-about-product">
                <p class="card-text about-product-price"><?= $product->get_price_html() ?></p>
                <?php
                $post = get_post(6);
                $guid_cart_page = $post->guid;
                ?>
                <div class="inner-about-product"
                     data-link="<?= $guid_cart_page . '&add-to-cart=' . $post->ID ?>">
                    <p class="card-text about-product-available ">Disponible</p>
                    <div class="wrapper-car-price">
                        <img src="<?= get_image_uri('shopping-cart.svg') ?>"
                             alt="">
                        <img class="white-cart d-none"
                             src="<?= get_image_uri('white-cart.svg') ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>

