<!-- Block  -->
<section class="block advantage page" id="advantage">
    <div class="container-fluid">
        <div class="holder">
            <div class="header">
                <div class="title-container">
                    <h2 class="title">Cubrimos tus necesidades de impresión al proveerte de toners, servicios y
                        el equipo que tu empresa necesita por una cuota mensual muy accesible.
                    </h2>
                </div>
            </div>
            <div class="content">
                <div class="title-content">
                    <h2 class="title">
                        Ventajas
                    </h2>
                </div>
                <div class="item-content">
                    <div class="row">
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon01.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon02.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon03.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon04.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon05.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 item-container">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?= get_image_uri('rent-icon06.svg') ?>" alt="" class="img-fluid">
                                </div>
                                <div class="desc">
                                    <div class="title">No pagas toner*</div>
                                    <div class="info">Nosotros nos encargamos de proveer todos los consumibles que se necesiten.
                                    </div>
                                </div>
                                <div class="small-mesage">
                                    *Impresiones limitadas
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /. -->