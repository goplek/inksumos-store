<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

global $product, $extraClass;
$args = array('post_type' => 'product', 'product_cat' => 'promociones', 'orderby' => 'title', 'order' => 'DESC');
$loop = new WP_Query($args);

?>
<?php
include "product-component.php";
?>