<!-- Block  -->
<section class="block contact page" id="contact">
    <div class="img-logo"><img src="wp-content/themes/inksumos/assets/images/logo-01.svg" alt="" class="img-fluid"></div>
    <div class="title-container">
        <div class="holder">
            <div class="tag">
                <b>Envíanos un mensaje para</b> solicitar más información.
            </div>
        </div>
    </div>
    <div class="form-container">
        <div class="container-fluid">
            <div class="holder">
                <div class="content">
                    <form class="advantage-contact">
                        <div class="input-container">
                            <div class="form-group has-rose bmd-form-group">
                                <label class="bmd-label-floating">Nombre</label>
                                <input name="Nombre" type="text" class="form-control">
                            </div>
                            <div class="form-group has-rose bmd-form-group">
                                <label class="bmd-label-floating">Correo*</label>
                                <input name="Correo" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group has-rose bmd-form-group">
                            <label class="bmd-label-floating">Mensaje*</label>
                            <textarea name="Mensaje" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-primary btn-rose btn-round center" type="submit">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /. -->