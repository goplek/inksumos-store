<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?>
<div class="site-branding">

    <?php if (has_custom_logo()) : ?>
        <div class="site-logo"><?php the_custom_logo(); ?></div>
    <?php endif; ?>
    <?php $blog_info = get_bloginfo('name'); ?>
    <?php if (has_nav_menu('menu-1')) : ?>

        <nav id="sectionsNav" class="navbar fixed-top navbar-expand-lg"
             aria-label="<?php esc_attr_e('Top Menu', 'inksumos'); ?>">
            <div class="container">
                <div class="navbar-translate">
                    <a href="/" class="navbar-brand">
                        <img src="<?= get_image_uri('logo-01.svg') ?>" alt="">
                    </a>
                    <button class="navbar-toggler">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_class' => 'main-menu',
                            'items_wrap' => '<ul id="%1$s" class="%2$s navbar-nav ml-auto">%3$s</ul>',
                        )
                    );
                    ?>
                </div>
            </div>
        </nav><!-- #site-navigation -->

    <?php endif; ?>
    <?php if (has_nav_menu('social')) : ?>
        <nav class="social-navigation" aria-label="<?php esc_attr_e('Social Links Menu', 'inksumos'); ?>">
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'social',
                    'menu_class' => 'social-links-menu',
                    'link_before' => '<span class="screen-reader-text">',
                    'link_after' => '</span>' . twentynineteen_get_icon_svg('link'),
                    'depth' => 1,
                )
            );
            ?>
        </nav><!-- .social-navigation -->
    <?php endif; ?>
</div><!-- .site-branding -->

<script>
    var elements = document.querySelectorAll('li');
    var links = document.querySelectorAll('li a');

    links.forEach(item => {
        item.classList.add('nav-link');
    });

    elements.forEach(element => {
        element.classList.add('nav-item');
    });
</script>