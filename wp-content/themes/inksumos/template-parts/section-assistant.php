<?php

/**
 * Created by PhpStorm.
 * User: goplek18
 * Date: 5/28/19
 * Time: 1:52 PM
 */

?>
<section class="wrapper-slider-assistant">
    <div class="prev-text directions d-none">
        <div class="inner-direction">Regresar</div>
    </div>
    <div class="slider assistant">
        <div class="slide">
            <div class="inner-assistant-thumb">
                <div class="assistant-title main">
                    Realiza nuestro test y descubre la impresora que se ajusta a tus necesidades.
                </div>
                <div class="assistant-subtitle">
                    Lo primero que debes definir es si requieres imprimir a color o solo en blanco y negro:
                </div>
                <div class="wrapper-information-test">
                    <div class="option">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option" value="Blanco y negro">
                                    b/n
                                    <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="content-value-radio"></div>
                            <div class="list-option">
                                <ul>
                                    <li>Para uso intenso</li>
                                    <li>Impresión muy económica</li>
                                    <li>Presentaciones planas por falta de color</li>
                                    <li>Ideal para oficinas, formatos, cartas o formularios</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="option second">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option"
                                           value="Color">Color
                                    <span class="circle">
                                            <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="list-option">
                                <ul>
                                    <li>Para usos visuales</li>
                                    <li>Impresiones impactantes</li>
                                    <li>Costo de impresión elevado</li>
                                    <li>Ideal para fotografías o reportes con gráficas</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="value-options d-none"></div>
            </div>
        </div>
        <div class="slide">
            <div class="inner-assistant-thumb">
                <div class="assistant-title">
                </div>
                <div class="assistant-subtitle">
                    Lo segundo que debes definir es la tecnología: láser o de inyección de tinta:
                </div>
                <div class="wrapper-information-test">
                    <div class="option">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label checked">
                                    <input class="form-check-input" type="radio" name="option"
                                           value="Toner lasér"> Láser
                                    <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="list-option">
                                <ul>
                                    <li>Para uso intenso</li>
                                    <li>Impresión rápida y de alta calidad</li>
                                    <li>Equipos de larga duración y uso intenso</li>
                                    <li>Costo de impresión más económico que la de tinta</li>
                                    <li>Inversión más alta en el equipo y consumibles</li>
                                    <li>Ideal para oficinas, corporativos o universidades</li>
                                    <li>Recomendable para más de 500 de impresiones al mes</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="option second">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option"
                                           value="Inyección de tinta">Tinta
                                    <span class="circle">
                                            <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="list-option">
                                <ul>
                                    <li>Para uso moderado</li>
                                    <li>Equipos económicos</li>
                                    <li>Equipos de corta vida útil</li>
                                    <li>Consumibles económicos</li>
                                    <li>Costo de impresión elevado</li>
                                    <li>Ideal para oficinas pequeñas y el hogar</li>
                                    <li>Recomendable para menos de 500 impresiones al mes</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="value-options d-none"></div>
            </div>
        </div>
        <div class="slide">
            <div class="inner-assistant-thumb">
                <div class="assistant-title">
                </div>
                <div class="assistant-subtitle">
                    Define si requieres escanear y sacar copias o únicamente imprimir:
                </div>
                <div class="wrapper-information-test">
                    <div class="option">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option"
                                           value="Impresora">Impresora
                                    <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="list-option">
                                <ul>
                                    <li>Recomendado para usuarios que no requieren más que solo imprimir
                                        (sin copias ni escaneos)
                                    </li>
                                    <li>Ideal para araea de nomina y contable, formatos estandarizados,
                                        reportes o bases de datos
                                    </li>
                                    <li>El costo del equipo y el del mantenimiento es mucho menor</li>
                                    <li>Está limitada a una sola función</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="option second">
                        <div class="inner-option">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option"
                                           value="Multifuncional">Multifuncional
                                    <span class="circle">
                                            <span class="check"></span>
                                            </span>
                                </label>
                            </div>
                            <div class="list-option">
                                <ul>
                                    <li>Estos equipos cuentan con función de impresora, copiadora y
                                        escaner
                                    </li>
                                    <li>Son muy versátiles y muy recomendables para todo tipo de usuarios,
                                        oficinas y hogar
                                    </li>
                                    <li> El costo del equipo y su mantenimiento es elevado</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="value-options d-none"></div>
            </div>
        </div>
        <div class="slide">
            <div class="inner-assistant-thumb">
                <div class="assistant-title">
                </div>
                <div class="assistant-subtitle">
                    Funciones adicionales:
                </div>
                <div class="wrapper-information-test additional">

                    <div class="form-check functions">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="option"
                                   value="Red alámbrica">Red alámbrica
                            <span class="circle">
                                                <span class="check"></span>
                                        </span>
                        </label>
                    </div>

                    <div class="form-check functions">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="option"
                                   value="Duplex">Duplex
                            <span class="circle">
                                                <span class="check"></span>
                                        </span>
                        </label>
                    </div>

                    <div class="form-check functions">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="option"
                                   value="Wifi">Wifi
                            <span class="circle">
                                                <span class="check"></span>
                                        </span>
                        </label>
                    </div>

                </div>
                <div class="value-options d-none"></div>
            </div>
        </div>
        <div class="slide" data-calculator="true">
            <div class="inner-assistant-thumb">
                <div class="assistant-title">
                </div>
                <div class="assistant-subtitle">
                    Rango de precio:
                </div>
                <div class="wrapper-information-test additional">

                    <div class="calculator">
                        <div class="content">
                            <!-- Side -->
                            <div class="side">
                                <div class="clearfix">
                                    <div class="range-slider"></div>
                                    <div class="min-value">$0</div>
                                    <div class="max-value">$10,000</div>
                                </div>
                            </div>
                            <!-- /.Side -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="next-text directions notAvailable">
        <div class="inner-direction">Siguiente</div>
    </div>
    <div class="search-element d-none">
        <div class="inner-direction">Buscar</div>
    </div>
</section>