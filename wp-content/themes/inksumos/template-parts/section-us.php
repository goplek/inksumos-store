<!-- Block Us -->
<section class="block us" id="us">
    <div class="img-logo"><img src="wp-content/themes/inksumos/assets/images/logo-01.svg" alt="" class="img-fluid">
    </div>
    <div class="holder">
        <div class="container-fluid">
            <div class="header">
                <div class="main-title">
                    <?php
                    $post = get_post(55);
                    ?>
                    <h2 class="title"><?= $post->post_title ?></h2>
                </div>
                <div class="info">
                    <?= $post->post_content ?>
                </div>
            </div>
            <div class="content">
                <div class="row">
                    <?php
                    $childrens = get_pages('child_of=' . $post->ID);
                    foreach ($childrens as $children) {
                        $post = get_post(96);
                        //$ref_of_we_products = $post->guid;
                        $ref_of_we_products = get_permalink();
                        $children_title = $children->post_title;
                        $opt1 = 'consumibles';
                        $opt2 = 'Impresión';
                        $new_guid = '';

                        if (strpos($children_title, $opt1) !== false) {
                            $new_guid = $ref_of_we_products . '/&' . 'cartuchos';
                        } else if (strpos($children_title, $opt2) !== false) {
                            $new_guid = $ref_of_we_products . '/&' . 'impresiones';
                        } else {
                            $post = get_post(209);
                            $new_guid = get_permalink();
                        }

                        $children_title = $children->post_title;
                        $children_content = $children->post_content;
                        $SearchPattern = '/<img (.+?)>/';
                        //reference : https://ayudawp.com/obtener-y-mostrar-imagenes-del-contenido/
                        //search pattern to quit images
                        $childrenDescription = preg_replace($SearchPattern, '', $children_content);
                        //apply filter to show the content well
                        $childrenDescription = apply_filters('the_content', $childrenDescription);

                        ?>
                        <div class="col-md-4 item">
                            <a href="<?= $new_guid ?>">
                                <div class="card card-player">
                                    <div class="logo col-md-6 ml-auto mr-auto">
                                        <?php
                                        if (preg_match_all($SearchPattern, $children_content, $matches)) {
                                            foreach ($matches[1] as $match) {
                                                foreach (wp_kses_hair($match, array('http')) as $attr)
                                                    $img[$attr['name']] = $attr['value'];
                                                $img_children_src = $img['src'];
                                                ?>
                                                <img src="<?= $img_children_src ?>" alt="" class="img-fluid">
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="desc">
                                        <div class="head"><?= $children_title ?></div>
                                        <div class="desc-description">
                                            <?= $childrenDescription ?>
                                        </div>
                                    </div>
                                    <button>Conoce más</button>
                                </div>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Us -->