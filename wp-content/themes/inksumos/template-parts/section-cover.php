<?php
/* BACKGROUND IMAGES */
$slide_image_1 = get_theme_mod('background_1');
$slide_image_2 = get_theme_mod('background_2');
$slide_image_3 = get_theme_mod('background_3');

/* ALT TEXT IMAGES */
$alt_text_1 = get_theme_mod('alt_text_1');
$alt_text_2 = get_theme_mod('alt_text_2');
$alt_text_3 = get_theme_mod('alt_text_3');

/* PRODUCT IMAGES */
$product_image_1 = get_theme_mod('image_1');
$product_image_2 = get_theme_mod('image_2');
$product_image_3 = get_theme_mod('image_3');

/* GET URL SITE */
$post = get_post(96);
//$url_site = $post->guid;
$url_site = get_permalink();

/* PRODUCT URL */
$product_url_1 = get_theme_mod('url_slide_1');
$url_explode1 = explode('=', $product_url_1);
$url_concat1 = $url_site . '#' . $url_explode1[1];

$product_url_2 = get_theme_mod('url_slide_2');
$url_explode2 = explode('=', $product_url_2);
$url_concat2 = $url_site . '#' . $url_explode2[1];

$product_url_3 = get_theme_mod('url_slide_3');
$url_explode3 = explode('=', $product_url_3);
$url_concat3 = $url_site . '#' . $url_explode3[1];

?>
<!-- Block  -->
<section class="block cover" id="portada">
    <div class="slider-content">

        <div class="search-bar-gplk">
            <?php get_search_form(); ?>
        </div>

        <div class="slide" style="background-image: url(<?= $slide_image_1 ?>)">
            <div class="content">
                <div class="image-cover">
                    <img src="<?= $product_image_1 ?>" alt="<?= $alt_text_1 ?>">
                </div>
                <a class="btn-gplk btn btn-rose btn-regular btn-round center" href="<?= $url_concat1 ?>">
                    <div class="text">Ver en la tienda</div>
                    <div class="arrw">
                        <img src="<?= get_image_uri('arrow-slider.svg') ?>">
                    </div>
                </a>
            </div>
        </div>

        <div class="slide" style="background-image: url(<?= $slide_image_2 ?>)">
            <div class="content">
                <div class="image-cover">
                    <img src="<?= $product_image_2 ?>" alt="<?= $alt_text_2 ?>">
                </div>
                <a class="btn-gplk btn btn-rose btn-regular btn-round center" href="<?= $url_concat2 ?>">
                    <div class="text">Ver en la tienda</div>
                    <div class="arrw">
                        <img src="<?= get_image_uri('arrow-slider.svg') ?>">
                    </div>
                </a>
            </div>
        </div>

        <div class="slide" style="background-image: url(<?= $slide_image_3 ?>)">
            <div class="content">
                <div class="image-cover">
                    <img src="<?= $product_image_3 ?>" alt="<?= $alt_text_3 ?>">
                </div>
                <a class="btn-gplk btn btn-rose btn-regular btn-round center" href="<?= $url_concat3 ?>">
                    <div class="text">Ver en la tienda</div>
                    <div class="arrw">
                        <img src="<?= get_image_uri('arrow-slider.svg') ?>">
                    </div>
                </a>
            </div>
        </div>

    </div>
    <div class="dots-content">
        <div class="dot-container"></div>
    </div>
</section>
<!-- /. -->

<script>
    var slides = document.querySelectorAll('.slide');
    var currentSlide = 0;
    var totalSlides = slides.length;

    var dotsContainer = document.querySelector('.dot-container');
    var interval;
    var dots;

    var slides_mob = document.querySelectorAll('.slide-mobile');

    function changeSlide() {
        for (var i = 0; i < totalSlides; i++) {
            slides[i].classList.remove('slide-fade');
        }

        slides[currentSlide].classList.add('slide-fade');
    }

    function changeDot() {
        for (var i = 0; i < totalSlides; i++) {
            dots[i].classList.remove('dot-white');
        }

        dots[currentSlide].classList.add('dot-white');
    }

    function updateCurrentSlide() {
        currentSlide = (currentSlide < totalSlides - 1)
            ? currentSlide + 1
            : 0;
    }

    function createDots() {
        for (var i = 0; i < totalSlides; i++) {
            (function (index) {
                var dot = document.createElement("span");
                dot.classList.add('dot-badge');
                dot.addEventListener("click", function () {
                    currentSlide = index;
                    changeSlide();
                    changeDot();

                    clearInterval(interval);
                    play();
                });

                dotsContainer.append(dot);
            })(i);
        }

        dots = dotsContainer.querySelectorAll('span');
    }

    function play() {
        interval = setInterval(function () {
            updateCurrentSlide();
            changeSlide();
            changeDot();
        }, 5000);
    }

    createDots();
    changeSlide();
    changeDot();
    play();
</script>