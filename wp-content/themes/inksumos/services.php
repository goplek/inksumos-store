<?php
/**
 * Template Name: Services
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>

<div class="outer-wrapper">

<?php
// Include Cover Section
get_template_part('template-parts/section-cover', 'services');

// Include Footer.
get_footer();
?>