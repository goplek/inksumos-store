<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
get_header();
?>
	<div class="outer-wrapper">
        <?php
        // Inlcude Cover Section
        get_template_part('template-parts/content/section', 'cover');
        // Inlcude Advantage Section
        get_template_part('template-parts/content/section', 'advantage');
        // Inlcude Form Section
        get_template_part('template-parts/content/section', 'contact');
        ?>
	</div>
<?php
get_footer();
