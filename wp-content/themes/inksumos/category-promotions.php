<?php
/**
 * Template Name: Promotions
 *
 * WooCommerce Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/*$args = array(
    'post_type' => 'cat',
);
$loop = new WP_Query( $args );*/

$parentid = get_queried_object_id();

$args = array(
    'parent' => $parentid
);

/*$categories =  get_categories(array('parent'    => $cat->cat_ID));

print_r($categories);


$result = "";

$args = array(
    'taxonomy' => "category",
    'parent' => $parent,
    'hide_empty' => 0
);

$categories = get_categories($args);

print_r($categories);*/





/*if ( $terms ) {

    echo '<ul class="product-cats">';

    foreach ( $terms as $term ) {

        echo '<li class="category">';

        woocommerce_subcategory_thumbnail( $term );

        echo '<h2>';
        echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
        echo $term->name;
        echo '</a>';
        echo '</h2>';

        echo '</li>';


    }

    echo '</ul>';

}*/


get_header();

?>

    <div class="outer-wrapper">

<?php

//Include Products Section
get_template_part('template-parts/section', 'cover-products');

//Include Products Section
get_template_part('template-parts/section', 'nav-products');

//Include Products Section
get_template_part('template-parts/section', 'content-products');


// Include Footer.
get_footer();
?>