<?php
    $UserMetaData = get_user_meta($user_id, USER_META_KEY);
    $user      = wp_get_current_user();
    $user_id   = get_current_user_id();
    $user_meta = get_user_meta($user_id);
    $orders    = get_orders_by_user_id();
    $storeMetaData = get_user_meta($user_id, STORE_META_KEY);

    if(($UserMetaData)) {
        $umd = json_decode($UserMetaData[0], true);
    }

    $fullname  = isset($umd) ? $umd['user_first_name'] . ' ' . $umd['user_last_name']:"";
    $cp        = isset($umd) ? 'Cp. ' . $umd['user_account_cp'] : "";
    $rfc       = isset($umd) ? 'RFC: ' . $umd['user_account_rfc']: "";
    $lada      = isset($umd) ? '('.$umd['user_account_lada'].') ':"";
    $phone     = isset($umd) ? $lada . $umd['user_account_phone']:"";
    $address   = isset($umd) ? $umd['user_account_address'] . ' ' . $umd['user_account_colony'] . ' ' . $umd['user_account_address'] . ', ' . $umd['user_account_city'] . ', ' . $umd['user_account_state']  :"";
    $colony    = isset($umd) ? $umd['user_account_colony'] : "";
    $street    = isset($umd) ? $umd['user_account_address']: "";
    $city      = isset($umd) ? $umd['user_account_city'] : "";
    $state     = isset($umd) ? $umd['user_account_state'] : "";
    $num_int   = isset($umd) ? $umd['user_account_num_int']:"";
    $num_ext   = isset($umd) ? $umd['user_account_num_ext']:"";


    // get the form fields
    $countries = new WC_Countries();
    $fields['shipping']['your_field']['options'] = array(
        'option_1' => 'Option 1 text',
        'option_2' => 'Option 2 text'
    );

    $shipping_fields = $countries->get_address_fields( '', 'shipping_' );
    $load_address = 'shipping';

?>
<div class="woocommerce-form-with-checkout">
    <form name="checkout" method="post" class="checkout woocommerce-checkout login-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
        <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
        <section class="data woocommerce-shipping-fields">
            <h2 class="title">
                Información de Pago:<span class="line-decoration"><span></span></span>
            </h2>
            <?php if(empty($UserMetaData)): ?>
                <div><a href="<?= get_permalink(wc_get_page_id('myaccount'))?>">Necesitas llenar tus datos</a></div>
            <?php else: ?>
            <div class="content row">
                <div class="general-data col-6 col-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
                            <p>
                                <?php echo esc_attr($fullname) ?>
                                <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text " name="billing_first_name" id="billing_first_name" placeholder="" value="<?php echo esc_attr($umd['user_first_name']) ?>" autocomplete="given-name"></span>
                                <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text "  name="billing_last_name" id="billing_last_name"  placeholder="" value="<?php echo esc_attr($umd['user_last_name']) ?>" autocomplete="family-name"></span>
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p><?php echo esc_attr($user->user_email)?></p>
                            <span class="woocommerce-input-wrapper hidden"><input type="email" class="input-text " name="billing_email" id="billing_email" placeholder="" value="<?php echo esc_attr__($user->user_email)?>" autocomplete="email username"></span>
                        </div>
                        <div class="col-sm-12">
                            <p><?php echo esc_attr($phone)?></p>
                            <span class="woocommerce-input-wrapper hidden"><input type="tel" class="input-text " name="billing_phone" id="billing_phone" placeholder="" value="<?php echo esc_attr($phone) ?>" autocomplete="tel"></span>
                        </div>
                        <span class="woocommerce-input-wrapper hidden"><strong>Mexico</strong><input type="hidden" name="billing_country" id="billing_country" value="MX" autocomplete="country" class="country_to_state" readonly="readonly"></span>
                    </div>
                </div>
                <div class="general-address col-6 col-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <p><?php echo $address ?></p>
                            <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text" name="billing_address_1" id="billing_address_1" placeholder="" value="<?php echo esc_attr($street)?>" autocomplete="address-line1"></span>
                            <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text " name="billing_address_2" id="billing_address_2" placeholder="" value="<?php echo esc_attr($colony)?>" autocomplete="address-line2"></span>
                            <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text" name="billing_city" id="billing_city" placeholder="" value="<?php echo esc_attr($city)?>" autocomplete="address-level2"></span>
                            <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text" name="billing_state" id="billing_state" placeholder="" value="<?php echo esc_attr($state)?>" autocomplete="address-level1">
                        </div>
                        <div class="col-sm-12">
                            <p><?php echo esc_attr($cp) ?></p>
                            <span class="woocommerce-input-wrapper hidden"><input type="text" class="input-text" name="billing_postcode" id="billing_postcode" placeholder="" value="<?php echo esc_attr($umd['user_account_cp']) ?>" autocomplete="postal-code"></span>
                        </div>
                    </div>
                </div>
                <div class="general-address col 12">
                    <div class="extra-info"><p><?php echo esc_attr($rfc) ?></p></div>
                </div>
                <div class="extra-fields hidden">
                    <span class="woocommerce-input-wrapper"><input type="text" class="input-text" name="shipping_int" id="shipping_int" placeholder="" value="<?php echo esc_attr($num_int)?>" autocomplete=""></span>
                    <span class="woocommerce-input-wrapper"><input type="text" class="input-text" name="shipping_ext" id="shipping_ext" placeholder="" value="<?php echo esc_attr($num_ext)?>" autocomplete=""></span>
                </div>
            </div>
            <?php endif; ?>
            <div class="input-container textarea-container">
                <p>Notas y observaciones</p>
                <textarea name="order_comments" class="input-text " id="order_comments" placeholder="Escribe tu mensaje*" rows="2" cols="5"></textarea>
            </div>
        </section>
        <?php include 'payment.php' ?>
        <div class="billing">
            <p>
                <label class="title" for="billing-confirm">¿Desea factura?</label>
                <input class="billing-check" type="checkbox" id="billing-confirm" name="billing-confirm">
            </p>
            <div class="billing-wrapper">
                <h2 class="title">¿Con qué RFC desea facturar?</h2>
                <div class="rfc-container check">
                    <ul>
                        <?php
                        $count = 0;
                        foreach($storeMetaData as $umdRFC):
                            $data = json_decode($umdRFC, true); ?>
                            <li>
                                <input class="rfc-check address-check" type="radio" id="select-rfc-<?= $count ?>" name="select-rfc" value="<?= $umd['user_account_rfc'] ?>">
                                <span class="checkmark"></span>
                                <label for="select-rfc-<?= $count ?>">
                                    <div class="address rfc">
                                        <p><?=  esc_attr($rfc) ?></p>
                                    </div>
                                </label>
                            </li>
                        <?php $count++;
                        endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="shipping-wrapper">
                <h2 class="title">
                    ¿A qué dirección deseas realizar el envío?<span class="line-decoration line-decoration-right"></span>
                </h2>
                <div class="input-hidden hidden">
                    <div class="woocommerce-shipping-fields">
                        <h3 id="ship-to-different-address">
                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="checkbox" name="ship_to_different_address" value="1">
                                <span>Ship to a different address?</span>
                            </label>
                        </h3>
                    </div>
                    <?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

                    <?php foreach ( $shipping_fields as $key => $field ) : ?>

                        <?php woocommerce_form_field( $key, $field); ?>

                    <?php endforeach; ?>

                    <?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>
                    <div class="extra-data">
                        <span class="woocommerce-input-wrapper"><input type="text" class="input-text" name="_shipping_rfc_name" id="_shipping_rfc_name" placeholder="" value="<?php echo esc_attr($fullname)?>" autocomplete=""></span>
                    </div>
                </div>
                <?php if(empty($storeMetaData)): ?>
                <div><a href="<?= get_permalink(wc_get_page_id('myaccount'))?>">Para poder realizar el pago, necesitas registrar una dirección</a></div>
                <?php else: ?>
                <div class="address form-address check">
                    <ul>
                        <?php
                            $count = 0;
                            foreach ($storeMetaData as $smd):
                            $data = json_decode($smd, true);
                            $address = isset($data) ? $data['store_shipping_address'] . ' ' . $data['store_shipping_address_colony'] . '. ' . $data['store_shipping_address_city']:"";
                        ?>
                            <li>
                                <input class="address-check" type="radio" id="select-address-<?= $count ?>" name="select-address"
                                       data-name = "<?=$data['store_shipping_attention_name']?>"
                                       data-last-name = "<?= $data['store_shipping_attention_last_name']?>"
                                       data-address-1 = "<?= $data['store_shipping_address'] ?>"
                                       data-address-2 = "<?= $data['store_shipping_address_colony'] ?>"
                                       data-city = "<?= $data['store_shipping_address_city'] ?>"
                                       data-state = "<?= $data['store_shipping_address_state'] ?>"
                                       data-postcode = "<?= $data['store_shipping_address_cp'] ?>"
                                       data-storename ="<?= $data['store_shipping_address_name'] ?>"
                                >
                                <span class="checkmark"></span>
                                <label for="select-address-<?= $count ?>">
                                    <div class="title"><?php echo esc_attr($data['store_shipping_address_name']) ?></div>
                                    <div class="address">
                                        <p><?php echo esc_attr($address); ?></p>
                                    </div>
                                </label>
                            </li>
                        <?php $count++;
                        endforeach; ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="terms-conditions form-group bmd-form-group form-check">
            <div class="input-group input-container">
                <input id="terms-conditions" name="Terminos" type="checkbox" value="Acepto términos y condiciones*"/>
                <label for="terms-conditions"><p>Acepto <b>términos y condiciones*</b></p></label>
            </div>
        </div>
        <div class="btn-wrpr">
            <a class="back-btn" href="javascript:history.back()">
                <span class="icon1">
                    <svg width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 52.5 (67469) - http://www.bohemiancoding.com/sketch-->
                        <title>left-arrow copy 8</title>
                        <desc>Created with Sketch.</desc>
                        <g id="10-MI-CARRITO" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Interna-Renta-de-equipo" transform="translate(-105.000000, -983.000000)" fill="#000000" fill-rule="nonzero">
                                <g id="left-arrow-copy-8" transform="translate(105.000000, 983.000000)">
                                    <path d="M3.93017915,7.66982759 C3.98713826,7.72327586 4.05833716,7.75 4.13665595,7.75 C4.21497474,7.75 4.28617363,7.72327586 4.34313275,7.66982759 C4.45705099,7.56293103 4.45705099,7.38922414 4.34313275,7.28232759 L0.711988976,3.875 L4.34313275,0.467672414 C4.45705099,0.360775862 4.45705099,0.187068966 4.34313275,0.0801724138 C4.22921452,-0.0267241379 4.04409738,-0.0267241379 3.93017915,0.0801724138 L0.0854386771,3.68125 C-0.028479559,3.78814655 -0.028479559,3.96185345 0.0854386771,4.06875 L3.93017915,7.66982759 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <span class="icon2">
                    <svg width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 52.5 (67469) - http://www.bohemiancoding.com/sketch-->
                        <title>left-arrow copy 8</title>
                        <desc>Created with Sketch.</desc>
                        <g id="10-MI-CARRITO" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Interna-Renta-de-equipo-Copy" transform="translate(-105.000000, -983.000000)" fill="#FE0061" fill-rule="nonzero">
                                <g id="left-arrow-copy-8" transform="translate(105.000000, 983.000000)">
                                    <path d="M3.93017915,7.66982759 C3.98713826,7.72327586 4.05833716,7.75 4.13665595,7.75 C4.21497474,7.75 4.28617363,7.72327586 4.34313275,7.66982759 C4.45705099,7.56293103 4.45705099,7.38922414 4.34313275,7.28232759 L0.711988976,3.875 L4.34313275,0.467672414 C4.45705099,0.360775862 4.45705099,0.187068966 4.34313275,0.0801724138 C4.22921452,-0.0267241379 4.04409738,-0.0267241379 3.93017915,0.0801724138 L0.0854386771,3.68125 C-0.028479559,3.78814655 -0.028479559,3.96185345 0.0854386771,4.06875 L3.93017915,7.66982759 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <span class="text">Regresar</span>
            </a>

            <div class="form-row place-order">
                <?php
                if(empty($storeMetaData) || empty($UserMetaData))
                    $disable_button = "disabled";
                else
                    $disable_button = "enable";
                ?>
                <button <?= $disable_button ?> type="submit" class="btn btn-primary btn-rose btn-round center my-btn" name="woocommerce_checkout_place_order" id="place_order" value="Place order" data-value="Place order">Pagar</button>
                <input type="hidden" id="woocommerce-process-checkout-nonce" name="woocommerce-process-checkout-nonce" value="9259da9429">
                <input type="hidden" name="_wp_http_referer" value="/?page_id=7">

                <?php do_action( 'woocommerce_review_order_after_submit' ); ?>

                <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>

            </div>
        </div>
    </form>
</div>

<script>

    var checkbox = document.getElementById('billing-confirm'),
        billingContent = document.querySelector('.billing-wrapper');

    checkbox.addEventListener("change", validaCheckbox, false);

    function validaCheckbox(){
        var checked = checkbox.checked;
        if(checked){
            billingContent.style.display = "block";
        }
        else{
            billingContent.style.display = "none";
        }
    }

</script>