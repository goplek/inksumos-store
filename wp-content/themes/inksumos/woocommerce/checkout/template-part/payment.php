<h2 class="title" id="order_review_heading"><?php esc_html_e( 'Selecciona tu método de pago:', 'woocommerce' ); ?><span class="line-decoration"><span></span></span></h2>

<div id="order_review" class="woocommerce-checkout-review-order">
    <div id="payment" class="woocommerce-checkout-payment">
        <ul class="wc_payment_methods payment_methods methods">
            <li class="wc_payment_method payment_method_paypal">
                <input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="paypal" checked="checked" data-order_button_text="Proceed to PayPal">

                <label for="payment_method_paypal">
                    <img src="<?= get_image_uri('Paypal.svg') ?>" alt="">
                </label>
            </li>
            <li class="wc_payment_method payment_method_bacs">
                <input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" data-order_button_text="">

                <label for="payment_method_bacs">
                    <img src="<?= get_image_uri('transfer.svg') ?>" alt="">
                </label>
            </li>
            <li class="wc_payment_method payment_method_sb_test">
                <input id="payment_method_sb_test" type="radio" class="input-radio" name="payment_method" value="sb_test" data-order_button_text="">
                <label for="payment_method_sb_test">
                    Test gateway 	</label>
            </li>
        </ul>
    </div>
</div>