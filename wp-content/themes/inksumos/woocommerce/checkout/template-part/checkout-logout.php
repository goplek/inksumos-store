<div class="woocommerce-form-checkout">
    <form name="checkout" method="post" class="checkout woocommerce-checkout checkout-logout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
        <?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
        <section class="data woocommerce-shipping-fields">
            <h2 class="title">
                Datos personales<span class="line-decoration"><span></span></span>
            </h2>
            <!--Billing Region -->
            <div class="woocommerce-shipping-fields__field-wrapper">
                <span class="woocommerce-input-wrapper hidden">
                    <input type="" name="billing_country" id="billing_country" value="MX" autocomplete="country" class="country_to_state" readonly="readonly">
                </span>
                <span>Nombre y apellidos</span>
                <div class="flex-container">
                    <p class="validate-required input-group input-container" id="billing_first_name" data-priority="10">
                        <input data-validate="range:3" type="text" class="input-text " name="billing_first_name" id="billing_first_name" placeholder="Nombre(s)" value="" autocomplete="given-name">
                    </p>
                    <p class="validate-required input-group input-container" id="billing_last_name" data-priority="20">
                        <input data-validate="notEmpty" type="text" class="input-text " name="billing_last_name" id="billing_last_name" placeholder="Apellidos" value="" autocomplete="family-name">
                    </p>
                </div>
                <div class="flex-container">
                    <p class="validate-required validate-email input-group input-container" id="billing_email" data-priority="110">
                        <input data-validate="email" type="email" class="input-text " name="billing_email" id="billing_email" placeholder="Correo" value="" autocomplete="email">
                    </p>
                </div>
                <h2 class="title">
                    Información de Pago<span class="line-decoration line-decoration-right"><span></span></span>
                </h2>
                <span>Calle y número</span>
                <div class="flex-container">
                    <p class="address-field input-group input-container" id="shipping_address_1" data-priority="50">
                        <input data-validate="notEmpty" placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_address_1" id="billing_address_1" autocomplete="" value="" />
                    </p>
                    <p class="input-group input-container small-input" id="billind_ext">
                        <input data-validate="notEmpty" placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_no_ext" id="billing_no_ext" autocomplete="" value="" />
                    </p>
                    <p class="input-group input-container small-input" id="shipping_int_field">
                        <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text " name="shipping_no_int" id="billing_no_int" autocomplete="" value="" />
                    </p>
                </div>
                <div class="flex-container">
                    <p class="input-group input-container">
                        <input data-validate="notEmpty" placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_address_2" id="billing_address_2" autocomplete="" value="" />
                    </p>
                    <p class="address-field input-group input-container" id="shipping_postcode_field" data-priority="90">
                        <input data-validate="notEmpty" placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_postcode" id="billing_postcode" autocomplete="" value="" />
                    </p>
                </div>
                <span>Ciudad y estado</span>
                <div class="flex-container">
                    <p class="address-field input-group input-container" id="shipping_city" data-priority="70">
                        <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_city" id="billing_city" autocomplete="" value="" />
                    </p>
                    <p class="address-field input-group input-container" id="shipping_state" data-priority="80">
                        <input data-validate="notEmpty" placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_state" id="billing_state" autocomplete="" value="" />
                    </p>
                </div>
                <span>Teléfono</span>
                <div class="flex-container">
                    <p class="input-group input-container" id="billing_phone" data-priority="100">
                        <input data-validate="range:8,15" placeholder="Teléfono" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_phone" id="billing_phone" autocomplete="" value="<?php echo esc_attr( $user->account_phone ); ?>" />
                    </p>
                </div>
                <span>Con atención a:</span>
                <div class="flex-container">
                    <p class="validate-required input-group input-container" id="" data-priority="10">
                        <input data-validate="notEmpty" type="text" class="input-text " name="shipping_first_name" id="shipping_first_name" placeholder="Nombre(s)" value="" autocomplete="given-name">
                    </p>
                    <p class="validate-required input-group input-container" id="shipping_last_name_field" data-priority="20">
                        <input data-validate="notEmpty" type="text" class="input-text " name="shipping_last_name" id="shipping_last_name" placeholder="Apellidos" value="" autocomplete="family-name">
                    </p>
                </div>
            </div>
            <!--End of Region-->

            <!--Notes and observations-->
            <div class="woocommerce-additional-fields__field-wrapper input-container textarea-container ">
                <span>Notas y observaciones</span>
                <textarea name="order_comments" class="input-text " id="order_comments" placeholder="" rows="2" cols="5"></textarea>
            </div>
            <!--End of Region-->

            <!--Payments-->
            <?php include 'payment.php' ?>
            <!--End of Region-->

            <!--Shipping Region -->
            <div class="shipping">
                <p>
                    <label class="title" for="billing-confirm">¿Desea factura?</label>
                    <input class="billing-check" type="checkbox" id="billing-confirm" name="billing-confirm">
                </p>
                <div class="billing-wrapper">
                    <div class="billing-info">
                        <span>Nombre o Razón social</span>
                        <p class="input-group input-container">
                            <input class="extra-field" name="_shipping_rfc_name" id="_shipping_rfc_name" type="text">
                        </p>
                        <span>RFC</span>
                        <p class="input-group input-container">
                            <input class="my-field-class form-row-wide custom_rfc" type="text" name="_shipping_rfc_field" id="_shipping_rfc_field">
                        </p>
                        <?php do_action( "woocommerce_before_checkout_shipping_form") ?>
                    </div>
                </div>
                <div class="billing-address">
                    <div id="ship-to-different-address" class="different-address">
                        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                            <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="checkbox" name="ship_to_different_address" value="1">
                            <h2>¿La dirección de envío es diferente a la de pago? <span class="line-decoration line-decoration-right"><span></span></span></h2>
                        </label>
                        <label class="label" for="ship-to-different-address-checkbox">sí</label>
                    </div>
                    <div class="billing-fields">
                        <h2 class="title">
                            Dirección de envío<span class="line-decoration"><span></span></span>
                        </h2>
                        <div class="billing-fields__field-wrapper">
                            <span>Calle y número</span>
                            <div class="flex-container">
                                <p class="validate-required input-group input-container" id="" data-priority="10">
                                    <input type="text" class="input-text" name="shipping_address_1" id="shipping_address_1" placeholder="Calle" value="" autocomplete="given-name">
                                </p>
                                <p class="validate-required input-group input-container" id="" data-priority="20">
                                    <input type="text" class="input-text " name="shipping_ext" id="shipping_ext" placeholder="Número exterior" value="" autocomplete="family-name">
                                </p>
                                <p class="validate-required input-group input-container">
                                    <input type="text" class="input-text" name="shipping_int" id="shipping_int" placeholder="Número interior" value="">
                                </p>
                            </div>
                            <div class="flex-container">
                                <p class="validate-required input-group input-container" id="shipping_address_2" data-priority="">
                                    <input type="text" placeholder="Colonia" name="shipping_address_2" id="shipping_address_2">
                                </p>
                                <p class="validate-required input-group input-container" id="" data-priority="">
                                    <input type="text" placeholder="C. P." name="shipping_postcode" id="shipping_postcode">
                                </p>
                            </div>
                            <span>Ciudad y estado</span>
                            <div class="flex-container">
                                <p class="validate-required input-group input-container" id="" data-priority="">
                                    <input type="text" placeholder="Ciudad" name="shipping_city" id="shipping_city">
                                </p>
                                <p class="validate-required input-group input-container" id="" data-priority="">
                                    <input type="text" placeholder="Estado" name="shipping_state" id="shipping_state">
                                </p>
                                <!--<p class="address-field input-group input-container" id="shipping_state" data-priority="80">
                                    <input data-validate="notEmpty" placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="shipping_country" id="shipping_country" autocomplete="" value="" />
                                </p>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <span class="woocommerce-input-wrapper hidden">
            <input type="" name="shipping_country" id="shipping_country" value="MX" autocomplete="country" class="country_to_state" readonly="readonly">
        </span>

        <div class="terms-conditions form-group bmd-form-group form-check">
            <div class="input-group input-container">
                <input id="terms-conditions" name="Terminos" type="checkbox" value="Acepto términos y condiciones*"/>
                <label for="terms-conditions">Acepto términos y condiciones*</label>
            </div>
        </div>

        <div class="btn-wrpr">
            <a class="back-btn" href="javascript:history.back()">
                <span class="icon1">
                    <svg width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 52.5 (67469) - http://www.bohemiancoding.com/sketch-->
                        <title>left-arrow copy 8</title>
                        <desc>Created with Sketch.</desc>
                        <g id="10-MI-CARRITO" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Interna-Renta-de-equipo" transform="translate(-105.000000, -983.000000)" fill="#000000" fill-rule="nonzero">
                                <g id="left-arrow-copy-8" transform="translate(105.000000, 983.000000)">
                                    <path d="M3.93017915,7.66982759 C3.98713826,7.72327586 4.05833716,7.75 4.13665595,7.75 C4.21497474,7.75 4.28617363,7.72327586 4.34313275,7.66982759 C4.45705099,7.56293103 4.45705099,7.38922414 4.34313275,7.28232759 L0.711988976,3.875 L4.34313275,0.467672414 C4.45705099,0.360775862 4.45705099,0.187068966 4.34313275,0.0801724138 C4.22921452,-0.0267241379 4.04409738,-0.0267241379 3.93017915,0.0801724138 L0.0854386771,3.68125 C-0.028479559,3.78814655 -0.028479559,3.96185345 0.0854386771,4.06875 L3.93017915,7.66982759 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <span class="icon2">
                    <svg width="5px" height="8px" viewBox="0 0 5 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 52.5 (67469) - http://www.bohemiancoding.com/sketch-->
                        <title>left-arrow copy 8</title>
                        <desc>Created with Sketch.</desc>
                        <g id="10-MI-CARRITO" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Interna-Renta-de-equipo-Copy" transform="translate(-105.000000, -983.000000)" fill="#FE0061" fill-rule="nonzero">
                                <g id="left-arrow-copy-8" transform="translate(105.000000, 983.000000)">
                                    <path d="M3.93017915,7.66982759 C3.98713826,7.72327586 4.05833716,7.75 4.13665595,7.75 C4.21497474,7.75 4.28617363,7.72327586 4.34313275,7.66982759 C4.45705099,7.56293103 4.45705099,7.38922414 4.34313275,7.28232759 L0.711988976,3.875 L4.34313275,0.467672414 C4.45705099,0.360775862 4.45705099,0.187068966 4.34313275,0.0801724138 C4.22921452,-0.0267241379 4.04409738,-0.0267241379 3.93017915,0.0801724138 L0.0854386771,3.68125 C-0.028479559,3.78814655 -0.028479559,3.96185345 0.0854386771,4.06875 L3.93017915,7.66982759 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <span class="text">Regresar</span>
            </a>
            <div class="form-row place-order">
                <button type="submit" class="btn btn-primary btn-rose btn-round center my-btn" name="woocommerce_checkout_place_order" id="place_order" value="Place order" data-value="Place order">Pagar</button>
                <input type="hidden" id="" name="woocommerce-process-checkout-nonce" value="9259da9429">
                <input type="hidden" name="_wp_http_referer" value="/?page_id=7">

                <?php do_action( 'woocommerce_review_order_after_submit' ); ?>

                <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>

            </div>
        </div>
        <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
    </form>
    <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

</div>