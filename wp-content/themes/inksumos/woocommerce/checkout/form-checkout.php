<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
$user      = wp_get_current_user();
$user_id   = get_current_user_id();
$user_meta = get_user_meta($user_id);

do_action( 'woocommerce_before_checkout_form', $checkout );

?>
<p><?php
    $edit_account = esc_url( wc_get_endpoint_url( 'edit-account','account-form' ) );
    $edit_address = esc_url( wc_get_endpoint_url( 'edit-account','address-form' ) );
    ?></p>

<?php
    if (!is_user_logged_in()) {
        include 'template-part/checkout-logout.php';
    }

    if (is_user_logged_in()) {
        include 'template-part/checkout-login.php';
    }

    do_action( 'woocommerce_after_checkout_form', $checkout );

?>

<script>
    var message = document.querySelectorAll('.woocommerce-info');
    message.forEach(msg => {
        msg.style.display = "none";
    });
</script>