<?php $template = get_query_var( 'edit-account');?>

<div class="flex-container">
    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container large-size">
        <label for="account_rfc_address"><?php esc_html_e( 'Calle y número', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        <input placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_address" id="account_rfc_address" autocomplete="" value="" />
    </p>
    <p class="woocommerce-form-row form-row input-container small-size">
        <input placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_ext" id="account_rfc_no_ext" autocomplete="" value="" />
    </p>
    <p class="woocommerce-form-row form-row input-container small-size">
        <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_int" id="account_rfc_no_int" autocomplete="" value="" />
    </p>
</div>
<div class="flex-container">
    <p class="woocommerce-form-row form-row input-container large-size">
        <input placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_colony" id="account_rfc_colony" autocomplete="" value="" />
    </p>
    <p class="woocommerce-form-row form-row input-container medium-size">
        <input placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_cp" id="account_rfc_cp" autocomplete="" value="" />
    </p>
</div>
<div class="flex-container">
    <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container large-size">
        <label for="account_rfc_address"><?php esc_html_e( 'Ciudad y estado', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
        <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_city" id="account_rfc_address" autocomplete="" value="" />
    </p>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container medium-size">
        <input placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_rfc_no_state" id="account_rfc_no_ext" autocomplete="" value="" />
    </p>
</div>
