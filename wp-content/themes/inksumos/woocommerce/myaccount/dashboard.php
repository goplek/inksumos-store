<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user      = wp_get_current_user();
$user_id   = get_current_user_id();
$user_meta = get_user_meta($user_id);
$orders    = get_orders_by_user_id();

//$meta_data = search_user_meta_data ( 'store_data' );
$storeMetaData = get_user_meta($user_id, STORE_META_KEY);
$UserMetaData = get_user_meta($user_id, USER_META_KEY);

if(($UserMetaData)) {
    $umd = json_decode($UserMetaData[0], true);
}
$fullname  = isset($umd) ? $umd['user_first_name'] . ' ' . $umd['user_last_name']:"";
$cp        = isset($umd) ? 'Cp. ' . $umd['user_account_cp'] : "";
$rfc       = isset($umd) ? 'RFC: ' . $umd['user_account_rfc']: "";
$lada      = isset($umd) ? '('.$umd['user_account_lada'].') ':"";
$phone     = isset($umd) ? $lada . $umd['user_account_phone']:"";
$address   = isset($umd) ? $umd['user_account_address'] . ' ' . $umd['user_account_colony'] . ' ' . $umd['user_account_address']:"";

?>
<p><?php
//		__( 'Opciones del Dashboard <a href="%1$s">Editar Cuenta</a> || <a href="%2$s">Editar Direcciones</a> || <a href="%3$s">Editar RFC</a>.', 'woocommerce' ),
		$edit_account = esc_url( wc_get_endpoint_url( 'edit-account','account-form' ) );
		$edit_address = esc_url( wc_get_endpoint_url( 'edit-account','address-form' ) );
?></p>
<div class="dashboard ">
    <section class="data">
        <h2>Mis Datos: <span><a href="<? echo $edit_account ?>">Editar</a></span></h2>
        <?php if($fullname && $cp && $rfc && $phone):  ?>
            <div class="content">
                <div class="general-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($fullname) ?></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($user->user_email) ?></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($phone) ?></div>
                        </div>
                    </div>
                </div>
                <div class="general-address">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($address) ?></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($user->account_colony) ?></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="info"><?php echo esc_attr($cp) ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
            <div class="general-data">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="info"><?php echo esc_attr($rfc) ?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php else: ?>
            <div class="">
                <div class="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class=""><?php echo esc_attr("Favor de Capturar tus datos completos")?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
    <hr>
    <section class="addressess">
        <h2>Mis Direcciones de envío:</h2><a class="add" href="<? echo $edit_address."&id_address=new"?>">Agregar dirección</a></span></h2>
        <?php if(count($storeMetaData) > 0): ?>
            <div class="container-address">
                <div class="height-address">
                    <?php foreach ($storeMetaData as $smd): ?>
                        <?php $data = json_decode($smd, true); ?>
                        <div class="content">
                            <div class="name"><?= esc_attr($data['store_shipping_address_name']) ?></div>
                            <div class="desc">
                                <div><?= esc_attr($data['store_shipping_address']) ?><span><a href="<?= $edit_address . "&id_address=" . $data['id'] ?>">Editar</a></span></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="arrow">
                <div class="img">
                    <img src="wp-content/themes/inksumos/assets/images/arrow.svg" alt="Arrow" class="img-fluid">
                </div>
            </div>
        <?php else:?>
            <div class="">
                <div class=""><?= esc_attr('No se han agregado direcciones de envío') ?></div>
            </div>
        <?php endif;?>
    </section>
    <hr>
    <section class="products">
        <h2>Mi historial de compra:</h2>
        <div class="wrapper">
            <ul class="products">
                <?php if(count($orders) > 0): ?>
                <?php
                    foreach ($orders as $order):
                        $orderID     = $order->ID;
                        $orderDate   = $order->post_date;
                        $orderStatus = $order->post_status;
                        $orderType   = $order->post_type;
                        $items = wc_get_order( $orderID );
                        foreach ($items->get_items() as $item_id => $item_data):
                            $product = $item_data->get_product(); // Get Product from Item
                            $product_sku = 'SKU '.$product->get_sku(); //Get Product SKU
                            $product_price = $product->get_price() ; //Get Product Price
                            $product_name = $product->get_name(); // Get the product name
                            $product_quantity = $item_data->get_quantity(); // Get the item quantity
                            $product_total = $item_data->get_total(); // Get the item line total
                            $product_thumb = get_the_post_thumbnail_url($product->get_id()); //Get thumbnail from each product
                            ?>
                            <li class="content">
                                <div class="product item">
                                    <div class="title">Producto</div>
                                    <div class="image-content">
                                        <div class="img">
                                            <img src="<?php echo $product_thumb ?>" alt="" class="img-fluid">
                                        </div>
                                        <div class="desc">
                                            <div class="first">Lorem Ipsum</div>
                                            <div class="second"><?php echo $product_sku ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="price item">
                                    <div class="title">Precio</div>
                                    <div class="desc"><?php echo $product_price ?></div>
                                </div>
                                <div class="quantity item">
                                    <div class="title">Cantidad</div>
                                    <div class="desc"><?php echo $product_quantity ?></div>
                                </div>
                                <div class="total item">
                                    <div class="title">Total</div>
                                    <div class="desc"><?php echo  $product_total ?></div>
                                </div>
                                <div class="date item">
                                    <div class="title">Fecha de Compra</div>
                                    <div class="desc"><?php echo $orderDate ?></div>
                                </div>
                                <div class="address item">
                                    <div class="title">Dirección de Envío</div>
                                    <div class="desc"><?php echo $address ?></div>
                                </div>
                            </li>
                            <hr></hr>
                            <?php echo woocommerce_pagination();  ?>
                        <?php endforeach;
                    endforeach;
                ?>
                <?php else: ?>
                    <div class="">
                        <div class="first"><?php echo esc_attr("No se han realizado compras")?></div>
                    </div>
                <?php endif; ?>
            </ul>
        </div>
    </section>
</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
