<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
$template = get_query_var( 'edit-account');
?>

<form class="woocommerce-EditAccountFor" action="" method="post" >
    <?php if($template === 'account-form') { ?>
    <?php wc_get_template_part('myaccount/forms/template','account') ?>
    <?php } ?>
    <?php if($template === 'rfc-form') { ?>
    <?php wc_get_template_part('myaccount/forms/template','rfc') ?>
    <?php } ?>
    <?php if($template === 'address-form') { ?>
    <?php wc_get_template_part('myaccount/forms/template','address') ?>
    <?php } ?>
    <div class="server-response">
        <!--//Enviando-->
        <div class="icon">
            <div class="text">
                <div class="info">
                    <img src="wp-content/themes/inksumos/assets/images/saving_data.svg" alt="Saving Data" class="img-fluid">
                </div>
                <div class="title"></div>
            </div>
        </div>
        <!--//Success-->
        <div class="icon">
            <div class="text">
                <div class="info"><img src="wp-content/themes/inksumos/assets/images/saved_data.svg" alt="Data Saved" class="img-fluid"></div>
                <div class="title"></div>
            </div>
        </div>
        <!--//Error-->
        <div class="icon">
            <div class="text">
                <div class="info"></div>
                <div class="title">
                    Ocurrió un Error. Inténtelo más tarde.
                </div>
            </div>
        </div>
    </div>
    <div class="modal-content d-none">
        <div class="content dummy-template">
            <div class="options">
                <div class="title">
                    ¿Deseas eliminar  esta dirección?
                </div>
                <div class="buttons-container">
                    <button type="button" class="woocommerce-Button button cancelar">Cancelar</button>
                    <button type="submit" class="woocommerce-Button button aceptar">Aceptar</button>
                </div>
                <div class="server-response">
                    <div class="state sending">Eliminando</div>
                    <div class="state error">Hubo un error al eliminar la dirección</div>
                    <div class="state success">La dirección ha sido eliminada</div>
                </div>
            </div>
        </div>
    </div>
    <p class="bottom-buttons">
        <a href="/?page_id=8"><?php esc_html_e( '< Regresar', 'woocommerce' ); ?></a>
        <button type="submit" class="woocommerce-Button button" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Guardar >', 'woocommerce' ); ?></button>
    </p>
</form>
