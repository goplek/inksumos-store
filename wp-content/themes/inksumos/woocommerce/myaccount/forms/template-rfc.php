<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25/03/19
 * Time: 10:05
 */
$user = wp_get_current_user();
$user_id = get_current_user_id();
?>
<div class="form-wrapper">
    <h1><?php esc_html_e( 'Agregar RFC:', 'woocommerce' ); ?></h1>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
            <label for="rfc_address_1"><?php esc_html_e( 'Calle y número', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input data-validate="notEmpty" placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_address_1" id="rfc_address_1" autocomplete="" value="<?php echo esc_attr($user->rfc_address_1) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container small-size">
            <input placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_address_num_ext_1" id="rfc_address_num_ext_1" autocomplete="" value="<?php echo esc_attr($user->rfc_address_num_ext_1) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container small-size">
            <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_address_num_int_1" id="rfc_address_num_int_1" autocomplete="" value="<?php echo esc_attr($user->rfc_address_num_int_1) ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row form-row input-container large-size">
            <input data-validate="notEmpty" placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_address_colony_1" id="rfc_address_colony_1" autocomplete="" value="<?php echo esc_attr($user->rfc_address_colony_1) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container medium-size">
            <input data-validate="notEmpty" placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_address_cp_1" id="rfc_address_cp_1" autocomplete="" value="<?php echo esc_attr($user->rfc_address_cp_1) ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
            <label for="shipping_address_city_1"><?php esc_html_e( 'Ciudad y estado', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
            <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_city_1" id="rfc_city_1" autocomplete="" value="<?php echo esc_attr( $user->rfc_city_1 ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container medium-size">
            <input placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="rfc_state_1" id="rfc_state_1" autocomplete="" value="<?php echo esc_attr( $user->rfc_state_1 ); ?>" />
        </p>
    </div>
</div>