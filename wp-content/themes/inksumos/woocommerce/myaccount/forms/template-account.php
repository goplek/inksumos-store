<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25/03/19
 * Time: 10:05
 */
$user = wp_get_current_user();
$user_id = get_current_user_id();
$user_metada = get_user_meta($user_id, USER_META_KEY);
if(($user_metada)) {
    $umd = json_decode($user_metada[0]);
}
$user_name    = isset($umd->user_first_name) ? $umd->user_first_name:"";
$last_name    = isset($umd->user_last_name) ? $umd->user_last_name:"";
$user_address = isset($umd->user_account_address) ? $umd->user_account_address:"";
$user_no_ext  = isset($umd->user_account_num_ext) ? $umd->user_account_num_ext:"";
$user_no_int  = isset($umd->user_account_num_int) ? $umd->user_account_num_int:"";
$user_col     = isset($umd->user_account_colony) ? $umd->user_account_colony:"";
$user_cp      = isset($umd->user_account_cp) ? $umd->user_account_cp:"";
$user_city    = isset($umd->user_account_city) ? $umd->user_account_city:"";
$user_state   = isset($umd->user_account_state) ? $umd->user_account_state:"";
$user_lada    = isset($umd->user_account_lada) ? $umd->user_account_lada:"";
$user_phone   = isset($umd->user_account_phone) ? $umd->user_account_phone:"";
$user_rfc_n   = isset($umd->user_account_rfc_name) ? $umd->user_account_rfc_name:"";
$user_rfc     = isset($umd->user_account_rfc) ? $umd->user_account_rfc:"";
?>
<div class="form-wrapper">
    <h1><?php esc_html_e( 'Editar mis datos:', 'woocommerce' ); ?></h1>
    <h2><?php esc_html_e( 'Datos Personales:', 'woocommerce' ); ?></h2>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container medium-size">
            <label for="first_name"><?php esc_html_e( 'Nombre y Apellidos', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
            <input data-validate="notEmpty" placeholder="Nombre (s)" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_first_name" id="first_name" autocomplete="" value="<?php echo (esc_attr($user_name)) ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container large-size">
            <input data-validate="notEmpty" placeholder="Apellidos" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_last_name" id="last_name" autocomplete="" value="<?php echo esc_attr( $last_name ); ?>" />
        </p>
    </div>
    <div class="clear"></div>

    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide full-size">
            <label for="account_display_name"><?php esc_html_e( 'Nombre de usuario', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_display_name" id="account_display_name" value="<?php echo esc_attr( $user->display_name ); ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container full-size">
            <label for="account_email"><?php esc_html_e( 'Correo', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input readonly data-validate="email" placeholder="Correo" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_email" id="account_email" autocomplete="" value="<?php echo esc_attr( $user->user_email ); ?>" />
        </p>
    </div>
    <div class="clear"></div>
    <!--<fieldset>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="password_current"><?php /*esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); */?></label>
            <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="user_password_current" id="password_current" autocomplete="off" />
        </p>
        <div class="flex-container">
            <p class="woocommerce-form-row form-row input-container half-size">
                <label for="password_1"><?php /*esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); */?></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="user_password_1" id="password_1" autocomplete="off" />
            </p>
            <p class="woocommerce-form-row form-row input-container half-size">
                <label for="password_2"><?php /*esc_html_e( 'Confirm new password', 'woocommerce' ); */?></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="user_password_2" id="password_2" autocomplete="off" />
            </p>
        </div>
    </fieldset>-->
    <h2><?php esc_html_e( 'Dirección', 'woocommerce' ); ?></h2>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
            <label for="account_address"><?php esc_html_e( 'Calle y número', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input data-validate="notEmpty" placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_address" id="account_address" autocomplete="" value="<?php echo esc_attr($user_address) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container small-size">
            <input placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_num_ext" id="account_num_ext" autocomplete="" value="<?php echo esc_attr($user_no_ext) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container small-size">
            <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_num_int" id="account_num_int" autocomplete="" value="<?php echo esc_attr($user_no_int) ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row form-row input-container large-size">
            <input data-validate="notEmpty" placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_colony" id="account_colony" autocomplete="" value="<?php echo esc_attr($user_col) ?>" />
        </p>
        <p class="woocommerce-form-row form-row input-container medium-size">
            <input data-validate="notEmpty" placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_cp" id="account_cp" autocomplete="" value="<?php echo esc_attr($user_cp) ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
            <label for="account_city"><?php esc_html_e( 'Ciudad y estado', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
            <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_city" id="account_city" autocomplete="" value="<?php echo esc_attr( $user_city ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container medium-size">
            <input placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_state" id="account_state" autocomplete="" value="<?php echo esc_attr( $user_state ); ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container small-size">
            <label for="account_phone"><?php esc_html_e( 'Teléfono', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
            <input data-validate="range:1,3" placeholder="Lada" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_lada" id="account_lada" autocomplete="" value="<?php echo esc_attr( $user_lada ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container extra-large-size">
            <input data-validate="phone" placeholder="Teléfono" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_phone" id="account_phone" autocomplete="" value="<?php echo esc_attr( $user_phone ); ?>" />
        </p>
    </div>
    <div class="flex-container">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container">
            <label for="account_first_name"><?php esc_html_e( 'Nombre o Razón social', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input placeholder="Nombre (s)" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_rfc_name" id="account_rfc_name" autocomplete="" value="<?php echo esc_attr( $user_rfc_n ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container">
            <label for="account_rfc"><?php esc_html_e( 'RFC', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input data-validate="rfcNumber" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_account_rfc" id="account_rfc" autocomplete="rfc" value="<?php echo esc_attr( $user_rfc ); ?>" />
        </p>
    </div>
    <div class="flex-container">
        <div id="cfdi_catalogue">
            <p class="form-row misha-field form-row-wide input-group input-container validate-required" id="cfdi_catalogue_field" data-priority="">
                <label for="_cfdi_catalogue" class="misha-label">Catálogo de uso de Comprobantes:<abbr class="required" title="required">*</abbr></label>
                <div class="woocommerce-input-wrapper">
                    <select name="_cfdi_catalogue" id="_cfdi_catalogue" class="select " data-allow_clear="true" data-placeholder="Selecciona:" name="user_cfdi_catalogue">
                        <option value="" selected="selected">Selecciona:</option>
                        <option value="Adquisición de mercancias">Adquisición de mercancias</option>
                        <option value="Devolucioenes y equipo de oficina por inversiones">Devolucioenes y equipo de oficina por inversiones</option>
                        <option value="Gastos en general">Gastos en general</option>
                        <option value="Construcciones">Construcciones</option>
                        <option value="Mobiliario y equipo de oficina por inversiones">Mobiliario y equipo de oficina por inversiones</option>
                        <option value="Equipo de transporte">Equipo de transporte</option>
                        <option value="Equipo de computo y accesorios">Equipo de computo y accesorios</option>
                        <option value="Dados, troqueles, moldes, matrices y herramental">Dados, troqueles, moldes, matrices y herramental</option>
                        <option value="Comunicaciones telefónicas">Comunicaciones telefónicas</option>
                        <option value="Comunicaciones satelitales">Comunicaciones satelitales</option>
                        <option value="Otra maquinaria y equipo">Otra maquinaria y equipo</option>
                        <option value="Honorarios médicos, dentales y gastos hospitalarios">Honorarios médicos, dentales y gastos hospitalarios</option>
                        <option value="Gastos médicos por incapacidad o discapacidad">Gastos médicos por incapacidad o discapacidad</option>
                        <option value="Gastos funerales">Gastos funerales</option>
                        <option value="Donativos">Donativos</option>
                        <option value="Intereses reales efectivamente pagados por créditos hipotecarios">Intereses reales efectivamente pagados por créditos hipotecarios</option>
                        <option value="Aportaciones voluntarios al SAR">Aportaciones voluntarios al SAR</option>
                        <option value="Primas por seguros de gastos médicos">Primas por seguros de gastos médicos</option>
                        <option value="Gastos de transportación escolar obligatoria">Gastos de transportación escolar obligatoria</option>
                        <option value="Depósitos en cuentas para el ahorro">Depósitos en cuentas para el ahorro</option>
                        <option value="Pagos por servicios educativos (colegiaturas)">Pagos por servicios educativos (colegiaturas)</option>
                        <option value="Por definir">Por definir</option>
                    </select>
                    <input data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="user_cfdi_catalogue" id="account_rfc" autocomplete="" value="" />
                </div>
            </p>
        </div>
    </div>
</div>