<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25/03/19
 * Time: 10:05
 */
$meta = get_user_meta(get_current_user_id());
$user = wp_get_current_user();
$user_id = get_current_user_id();
$id_address = $_GET['id_address'];
$flag = false;
if($id_address === 'new') {
    $address_name = '';
    $address = '';
    $address_num_ext = '';
    $address_num_int = '';
    $address_col = '';
    $address_cp = '';
    $address_city = '';
    $address_state = '';
    $address_lada = '';
    $address_phone = '';
    $address_attention_name = '';
    $address_attention_last = '';
    $address_rfc_name = '';
    $address_rfc_number = '';
    $flag = true;
}
else {
    $store_metada = get_user_meta($user_id, STORE_META_KEY);
    foreach ($store_metada as $smd):
        $data = json_decode($smd, true);
        if(intval($data['id']) === intval($id_address)):
            $address_name = $data['store_shipping_address_name'];
            $address = $data['store_shipping_address'];
            $address_num_ext = $data['store_shipping_address_num_ext'];
            $address_num_int = $data['store_shipping_address_num_int'];
            $address_col = $data['store_shipping_address_colony'];
            $address_cp = $data['store_shipping_address_cp'];
            $address_city = $data['store_shipping_address_city'];
            $address_state = $data['store_shipping_address_state'];
            $address_lada = $data['store_shipping_address_lada'];
            $address_phone = $data['store_shipping_address_phone'];
            $address_attention_name = $data['store_shipping_attention_name'];
            $address_attention_last = $data['store_shipping_attention_last_name'];
            $address_rfc_name = $data['store_rfc_name'];
            $address_rfc_number = $data['store_rfc_number'];
            $flag=true;
        endif;
    endforeach;
}

?>
<div class="form-wrapper">
    <h1><?php esc_html_e( 'Agregar Dirección:', 'woocommerce' ); ?></h1>
    <?php if($id_address !== 'new' && isset($address_name)): ?>
    <div class="main-title">
        <h2><?php echo esc_attr($address_name); ?></h2>
        <button type="button" class="delete"></button>
    </div>
    <?php endif ?>
    <?php if($flag): ?>
    <div class="container-form">
        <div class="flex-container d-none">
            <input placeholder="id" data-validate="notEmpty" type="text" class="input-text woocommerce-Input woocommerce-Input--text input-text" name="id" id="id" value="<?php echo esc_attr($id_address) ?>" />
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container full-size">
                <label for="shipping_address_name_1"><?php esc_html_e( 'Nombre de la dirección:', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input placeholder="Nombre" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_name" id="shipping_address_name_1" autocomplete="" value="<?php echo esc_attr($address_name) ?>" />
            </p>
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
                <label for="shipping_address_1"><?php esc_html_e( 'Calle y número', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input data-validate="notEmpty" placeholder="Calle" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address" id="shipping_address_1" autocomplete="" value="<?php echo esc_attr($address) ?>" />
            </p>
            <p class="woocommerce-form-row form-row input-container small-size">
                <input placeholder="Número Exterior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_num_ext" id="shipping_address_num_ext_1" autocomplete="" value="<?php echo esc_attr($address_num_ext) ?>" />
            </p>
            <p class="woocommerce-form-row form-row input-container small-size">
                <input placeholder="Número Interior" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_num_int" id="shipping_address_num_int_1" autocomplete="" value="<?php echo esc_attr($address_num_int) ?>" />
            </p>
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row form-row input-container large-size">
                <input data-validate="notEmpty" placeholder="Colonia" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_colony" id="shipping_address_colony_1" autocomplete="" value="<?php echo esc_attr($address_col) ?>" />
            </p>
            <p class="woocommerce-form-row form-row input-container medium-size">
                <input data-validate="notEmpty" placeholder="C.P." type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_cp" id="shipping_address_cp_1" autocomplete="" value="<?php echo esc_attr($address_cp) ?>" />
            </p>
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container large-size">
                <label for="shipping_address_city_1"><?php esc_html_e( 'Ciudad y estado', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
                <input data-validate="notEmpty" placeholder="Ciudad" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_city" id="shipping_address_city_1" autocomplete="" value="<?php echo esc_attr( $address_city ); ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container medium-size">
                <input placeholder="Estado" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_state" id="shipping_address_state_1" autocomplete="" value="<?php echo esc_attr( $address_state ); ?>" />
            </p>
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container small-size">
                <label for="shipping_address_lada_1"><?php esc_html_e( 'Teléfono', 'woocommerce' );?>&nbsp;<span class="required">*</span></label>
                <input data-validate="range:1,3" placeholder="Lada" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_lada" id="shipping_address_lada_1" autocomplete="" value="<?php echo esc_attr( $address_lada ); ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container extra-large-size">
                <input data-validate="phone" placeholder="Teléfono" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_address_phone" id="shipping_address_phone_1" autocomplete="" value="<?php echo esc_attr( $address_phone ); ?>" />
            </p>
        </div>
        <div class="flex-container">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first input-container medium-size">
                <label for="shipping_attention_name_1"><?php esc_html_e( 'Con atención a:', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input placeholder="Nombre (s)" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_attention_name" id="shipping_attention_name_1" autocomplete="" value="<?php echo esc_attr( $address_attention_name ); ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container large-size">
                <input placeholder="Apellidos" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_shipping_attention_last_name" id="shipping_attention_last_name_1" autocomplete="rfc" value="<?php echo esc_attr( $address_attention_last ); ?>" />
            </p>
        </div>
        <h1><?php esc_html_e( 'Agregar RFC:', 'woocommerce' ); ?></h1>
        <div class="flex-containet">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first full-size input-container">
                <label for="rfc_name_1"><?php esc_html_e( 'Nombre o Razón social', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input placeholder="Nombre (s)" data-validate="notEmpty" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_rfc_name" id="rfc_name_1" autocomplete="" value="<?php echo esc_attr( $address_rfc_name ); ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide input-container">
                <label for="rfc_number_1"><?php esc_html_e( 'RFC', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input data-validate="rfcNumber" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="store_rfc_number" id="rfc_number_1" autocomplete="rfc" value="<?php echo esc_attr( $address_rfc_number ); ?>" />
            </p>
        </div>
    </div>
    <?php else: ?>
    <div class="container-form false">
        <div class="content-return">
            El Registro no existe
        </div>
        <p class="bottom-buttons">
            <a href="/?page_id=8"><?php esc_html_e( '< Regresar', 'woocommerce' ); ?></a>
        </p>
    </div>
    <?php endif; ?>
</div>