<?php

// Simulate time that server takes to send a message.
sleep(5);

// Simulate response from server
// 1 = Success
// 0 = Error
echo 1;