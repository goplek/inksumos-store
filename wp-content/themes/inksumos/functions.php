<?php
/**
 * Inksumos Store functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Insksumos_Store
 * @since 1.0
 */

define('CSS_PATH', sprintf('%s/assets/css/', get_template_directory_uri()));
define('IMGS_PATH', sprintf('%s/assets/images/', get_template_directory_uri()));
define('JS_PATH', sprintf('%s/assets/js/', get_template_directory_uri()));

define('STORE_META_KEY', 'store_data');
define('USER_META_KEY', 'user_data');

function unicode_decode($str) {
    return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
}

/**
 * Inksumos Store only works in WordPress 5.1 or later.
 */
if (version_compare($GLOBALS['wp_version'], '5.1', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
    return;
}

/**
 * Parse string to slug form.
 * @param $text
 * @return string
 */
function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function inksumosstore_setup()
{

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(
        array(
            'menu-1' => __('Primary', 'inksumos'),
            'footer' => __('Footer Menu', 'inksumos'),
            'social' => __('Social Links Menu', 'inksumos'),
        )
    );

    // Set the default content width.
    $GLOBALS['content_width'] = 1300;

}

add_action('after_setup_theme', 'inksumosstore_setup');

/**
 * Enqueues scripts and styles.
 *
 * @since Inksumos Store 1.0
 */
function inksumosstore_scripts()
{

    // Material Kit stylesheet.
    wp_enqueue_style('material-kit-css', get_stylesheet_directory_uri() . '/assets/css/vendor/material-kit.min.css');

    // Theme stylesheet.
    wp_enqueue_style('inksumosstore-style', get_stylesheet_uri());

}

add_action('wp_enqueue_scripts', 'inksumosstore_scripts');


/**
 * Get absolute path for an image.
 *
 * @author cesarmejia
 * @param string $image_name
 * @return string
 */
function get_image_uri($image_name)
{
    return IMGS_PATH . $image_name;
}

function cover_information($wp_customize)
{
    // Se crea un panel
    $wp_customize->add_panel('cover', array(
        'title' => __('Cover', 'textdomain'),
        'description' => __('Aquí puede ir un mensaje', 'textdomain'),
        'priority' => 160,
        'capability' => 'edit_theme_options',
    ));

    //Se crean la(s) seccion(es)
    $wp_customize->add_section('cover_slide_1', array(
        'title' => __('Slide 1', 'textdomain'),
        'panel' => 'cover',
        'priority' => 201,
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_section('cover_slide_2', array(
        'title' => __('Slide 2', 'textdomain'),
        'panel' => 'cover',
        'priority' => 202,
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_section('cover_slide_3', array(
        'title' => __('Slide 3', 'textdomain'),
        'panel' => 'cover',
        'priority' => 202,
        'capability' => 'edit_theme_options',
    ));

    //CAMPOS PARA SLIDE 1

    //BACKGROUND SLIDE 1
    $wp_customize->add_setting('background_1', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'background_1',
            array(
                'label' => 'Background Slide',
                'settings' => 'background_1',
                'section' => 'cover_slide_1'
            )
        )
    );

    //IMAGEN SLIDE 1
    $wp_customize->add_setting('image_1', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'image_1',
            array(
                'label' => 'Imagen Slide',
                'settings' => 'image_1',
                'section' => 'cover_slide_1'
            )
        )
    );

    //URL SLIDE 1
    $wp_customize->add_setting('url_slide_1', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('url_slide_1', array(
        'label' => __('URL', 'textdomain'),
        'section' => 'cover_slide_1',
        'priority' => 1,
        'type' => 'text',
    ));

    //TEXTO ALTERNATIVO PARA IMAGEN 1

    $wp_customize->add_setting('alt_text_1', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('alt_text_1', array(
        'label' => __('Alt text', 'textdomain'),
        'section' => 'cover_slide_1',
        'priority' => 1,
        'type' => 'text',
    ));

    //CAMPOS PARA SLIDE 2

    //BACKGROUND SLIDE 2
    $wp_customize->add_setting('background_2', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'background_2',
            array(
                'label' => 'Background Slide',
                'settings' => 'background_2',
                'section' => 'cover_slide_2'
            )
        )
    );

    //IMAGEN SLIDE 2
    $wp_customize->add_setting('image_2', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'image_2',
            array(
                'label' => 'Imagen Slide',
                'settings' => 'image_2',
                'section' => 'cover_slide_2'
            )
        )
    );

    //URL SLIDE 2
    $wp_customize->add_setting('url_slide_2', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('url_slide_2', array(
        'label' => __('URL', 'textdomain'),
        'section' => 'cover_slide_2',
        'priority' => 1,
        'type' => 'text',
    ));

    //TEXTO ALTERNATIVO PARA IMAGEN 2

    $wp_customize->add_setting('alt_text_2', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('alt_text_2', array(
        'label' => __('Alt text', 'textdomain'),
        'section' => 'cover_slide_2',
        'priority' => 1,
        'type' => 'text',
    ));

    //CAMPOS PARA SLIDE 3

    //BACKGROUND SLIDE 3
    $wp_customize->add_setting('background_3', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'background_3',
            array(
                'label' => 'Background Slide',
                'settings' => 'background_3',
                'section' => 'cover_slide_3'
            )
        )
    );

    //IMAGEN SLIDE 3
    $wp_customize->add_setting('image_3', array(
        'default' => '',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'image_3',
            array(
                'label' => 'Imagen Slide',
                'settings' => 'image_3',
                'section' => 'cover_slide_3'
            )
        )
    );

    //URL SLIDE 3
    $wp_customize->add_setting('url_slide_3', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('url_slide_3', array(
        'label' => __('URL', 'textdomain'),
        'section' => 'cover_slide_3',
        'priority' => 1,
        'type' => 'text',
    ));

    //TEXTO ALTERNATIVO PARA IMAGEN 3

    $wp_customize->add_setting('alt_text_3', array(
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('alt_text_3', array(
        'label' => __('Alt text', 'textdomain'),
        'section' => 'cover_slide_3',
        'priority' => 1,
        'type' => 'text',
    ));
}

add_action('customize_register', 'cover_information');

add_filter('wp_nav_menu_items', 'buscador_en_menu', 10, 2);

function buscador_en_menu($items, $args)
{

    $searchform = get_search_form(false);

    $items .= '<li>' . $searchform . '</li>';
    return $items;
}

add_filter('wp_nav_menu_items', 'dcms_items_login_logout', 10, 2);

function dcms_items_login_logout($items, $args) {
    if ($args->theme_location == 'menu-1') {
        if (is_user_logged_in()) {
            $items .= '<ul class="list-logout">
                            <li class="menu-item btn-menu btn-logout item-logout">
						        <a href="' .get_permalink(wc_get_page_id('myaccount')). '">Mi Cuenta</a>
						    </li>
						    <li class="menu-item btn-menu btn-logout item-logout">
						        <a href="'.  wp_logout_url( home_url()). '">Cerrar Sesión</a>
						    </li>
                        </ul>';
        }
        else
        {
            $items .= '<ul class="list-login">
                            <li class="item-login signup-btn">                              
                                <a href="' . get_permalink(wc_get_page_id('myaccount')). '?action=#register">Crear cuenta</a>                               
                            </li>
                            <li class="item-login log-in-btn">
                                <a href="' . get_permalink(wc_get_page_id('myaccount')). '?action=#login">Mi cuenta</a>
                            </li>
                        </ul>';
        }
    }

    return $items;
}

function my_custom_register_redirect()
{

    $register_url = "?p=49";
    return $register_url;
}

add_action('register_url', 'my_custom_register_redirect');

function my_custom_login_redirect()
{

    wp_redirect(home_url("?p=49"));

    exit();
}

add_action('wp_login', 'my_custom_login_redirect');

function my_custom_logout_redirect()
{
    wp_redirect(home_url("?p=49"));
    exit();
}

add_action('wp_logout', 'my_custom_logout_redirect');
// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error

function bbloomer_add_form_endpoints()
{
    add_rewrite_endpoint('rfc-form', EP_ROOT | EP_PAGES);
    add_rewrite_endpoint('address-form', EP_ROOT | EP_PAGES);
    add_rewrite_endpoint('account-form', EP_ROOT | EP_PAGES);
}

add_action('init', 'bbloomer_add_form_endpoints');

// ------------------
// 2. Add new query var

function bbloomer_rfc_form_query_vars($vars)
{
    $vars[] = 'rfc-form';
    return $vars;
}

add_filter('query_vars', 'bbloomer_rfc_form_query_vars', 0);

// ------------------
// 3. Insert the new endpoint into the My Account menu

function bbloomer_add_form_link_my_account($items)
{
    $items['rfc-form'] = 'RFC Form';
    $items['address-form'] = 'Address Form';
    $items['account-form'] = 'Account Form';
    return $items;
}

add_filter('woocommerce_account_menu_items', 'bbloomer_add_form_link_my_account');

//// ------------------
//// 4. Add content to the new endpoint
//
function bbloomer_form_content()
{
    wc_get_template('myaccount/form-edit-account.php');
}

add_action('woocommerce_account_form_endpoint', 'bbloomer_form_content');

//// ------------------
//// Goplek Request to save data forms.
//
function goplek_request()
{
    global $wpdb;
    $data = $_POST['data'];
    $id_address = $data['id'];
    $userId = get_current_user_id();
    $userData = array();
    $currentUserMetaData = get_user_meta($userId, USER_META_KEY);
    $currentStoreMetaData = get_user_meta($userId, STORE_META_KEY);

    $userMetaData = array();
    $storeMetaData = array();
    $table_name = $wpdb->prefix . 'usermeta';

    // region Update User Data
    if (array_key_exists('user_display_name', $data)) {
        $userData['display_name'] = $data['user_display_name'];
        unset($data['user_display_name']);
    }

    if (array_key_exists('user_email', $data)) {
        $userData['user_email'] = $data['user_email'];
        unset($data['user_email']);
    }

    if (!empty($userData)) {
        $userData['ID'] = $userId;
        wp_update_user($userData);
    }
    // endregion

    // region Update User Meta Data
    foreach ($data as $key => $val) {
        if (strpos($key, 'user_') !== FALSE) {
            $userMetaData[$key] = $val;
        }
    }


    if (!empty($userMetaData)) {
        if (empty($currentUserMetaData)) {
            add_user_meta($userId, USER_META_KEY, json_encode($userMetaData));
        } else {
            update_user_meta($userId, USER_META_KEY, json_encode($userMetaData));
        }
    }
    // endregion

    // region Update Store Data
    foreach ($data as $key => $val) {
        if (strpos($key, 'store_') !== FALSE) {
            $storeMetaData[$key] = $val;
        }
    }

    if (!empty($storeMetaData)) {
        if (empty($currentStoreMetaData)) {
            $storeMetaData = array_merge(array('id' => 1), $storeMetaData);
            add_user_meta($userId, STORE_META_KEY, json_encode($storeMetaData));
        } elseif (array_key_exists('id', $data) && $id_address !== 'new') {
            $myrecord = $wpdb->get_results("SELECT umeta_id, meta_value FROM wp_usermeta WHERE meta_key = 'store_data' AND user_id = $userId ");
            foreach ($myrecord as $value):
                if(intval($data['id']) === intval(json_decode($value->meta_value)->id)) {
                    $storeMetaData = array_merge(array('id' => $data['id']), $storeMetaData);
                    $output = json_encode($storeMetaData);
                    $target_id = intval($value->umeta_id);
                }
            endforeach;
            // region Update Store Data
            if(isset($target_id) && isset($output))
                $wpdb->update($table_name, array( 'meta_value' => $output), array('umeta_id' => $target_id));
        } else {
            // Retrieve the last id inserted of stores meta data.
            $lastId = 0;
            foreach ($currentStoreMetaData as $smd) {
                $buffer = json_decode($smd, true);
                if (array_key_exists('id', $buffer)) {
                    $lastId = intval($buffer['id']);
                }
            }
            $storeMetaData = array_merge(array('id' => $lastId + 1), $storeMetaData);
            add_user_meta($userId, STORE_META_KEY, json_encode($storeMetaData));
        }
    }
    // endregion
    die;
}

add_action('wp_ajax_upsert_usermetadata', 'goplek_request');

function wp_ajax_delete_usermetadata()
{
    global $wpdb;
    $userId = get_current_user_id();
    $table_name = $wpdb->prefix . 'usermeta';
    $page = 'admin.php?page=my_plugins_settings_page';
    if (isset($_POST['id'])) {
        $myrecord = $wpdb->get_results("SELECT umeta_id, meta_value FROM $table_name WHERE meta_key = 'store_data' AND user_id = $userId");
        foreach ($myrecord as $value):
            if (intval($_POST['id']) === intval(json_decode($value->meta_value)->id)) {
                $wpdb->delete($table_name, array('umeta_id' => $value->umeta_id));
            }
        endforeach;
    }
    die;
}

add_action('wp_ajax_delete_usermetadata', 'wp_ajax_delete_usermetadata');


/**
 * Get all customer orders
 *
 * @author alejandrodelgadillo
 * @return array
 *
 **/
function get_orders_by_user_id()
{
    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => get_current_user_id(),
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()),  //'post_status' => array('wc-completed', 'wc-processing'),
    ));

    return $customer_orders;
}

/**
* @snippet       WooCommerce Set Default City @ Checkout
*/

add_filter( 'woocommerce_checkout_fields', 'set_checkout_field_input_value_default' );

function set_checkout_field_input_value_default($fields) {
    $fields['shipping']['shipping_country']['default'] = 'Mexico';
    return $fields;
}

function xa_filter_woocommerce_states( $states ) {
    unset($states['MX']);
    return $states;
};
add_filter( 'woocommerce_states', 'xa_filter_woocommerce_states', 10, 1 );

/**
 * @snippet       WooCommerce Set Default City @ Checkout
 */

add_filter( 'woocommerce_checkout_fields', 'set_billing_field_input_value_default' );

function set_billing_field_input_value_default($fields) {
    $fields['billing']['billing_country']['default'] = 'Mexico';
    return $fields;
}

function xa_filter_woocommerce_billing_states( $states ) {
    unset($states['MX']);
    return $states;
};
add_filter( 'woocommerce_states', 'xa_filter_woocommerce_billing_states', 10, 1 );

/**
 *  Add Custom RFC field on User with login Checkout
 */
add_action( 'woocommerce_checkout_before_customer_details', 'custom_checkout_fields_before_billing_details', 20 );
function custom_checkout_fields_before_billing_details(){
    $domain = 'woocommerce';
    $checkout = WC()->checkout;
    echo '<div id="rfc_shipping_custom_checkout_field">';
    woocommerce_form_field( '_shipping_rfc_field', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide custom_rfc hidden'),
        'required'      => true, // or false
    ), $checkout->get_value( '_shipping_rfc_field' ) );

    echo '</div>';
}

// Save custom RFC field on User with Login Checkout
add_action( 'woocommerce_checkout_create_order', 'custom_checkout_field_update_meta', 10, 2 );
function custom_checkout_field_update_meta( $order, $data ){
    if( isset($_POST['_shipping_rfc_field']) && ! empty($_POST['_shipping_rfc_field']) )
        $order->update_meta_data( '_shipping_rfc_field', sanitize_text_field( $_POST['_shipping_rfc_field'] ) );
}

// Shipping Extra fields
function misha_subscribe_checkbox( $checkout ) {
    woocommerce_form_field( '_shipping_rfc_name', array(
        'type'	=> 'text',
        'required' => 'true',
        'class'	=> array('extra-field form-row-wide test-input'),
    ), $checkout->get_value( '_shipping_rfc_name' ) );

    woocommerce_form_field( '_shipping_rfc_field', array(
        'type'	=> 'text',
        'required' => 'true',
        'class'	=> array('extra-field form-row-wide test-input'),
    ), $checkout->get_value( '_shipping_rfc_field' ) );

    woocommerce_form_field( 'shipping_ext', array(
        'type'	=> 'text',
        'required' => 'true',
        'class'	=> array('extra-field form-row-wide test-input'),
    ), $checkout->get_value( 'shipping_ext' ) );

    woocommerce_form_field( 'shipping_int', array(
        'type'	=> 'text',
        'required' => 'true',
        'class'	=> array('extra-field form-row-wide test-input'),
    ), $checkout->get_value( 'shipping_int' ) );

}
// save field values
add_action( 'woocommerce_checkout_create_order', 'misha_save_what_we_added', 10, 2 );
function misha_save_what_we_added($order, $data ){
    if( isset($_POST['_shipping_rfc_name']) && ! empty($_POST['_shipping_rfc_name']) )
        $order->update_meta_data( '_shipping_rfc_name', sanitize_text_field( $_POST['_shipping_rfc_name'] ) );
    if( isset($_POST['_shipping_rfc_field']) && ! empty($_POST['_shipping_rfc_name']) )
        $order->update_meta_data( '_shipping_rfc_field', sanitize_text_field( $_POST['_shipping_rfc_field'] ) );
    if( isset($_POST['shipping_ext']) && ! empty($_POST['shipping_ext']) )
        $order->update_meta_data( 'shipping_ext', sanitize_text_field( $_POST['shipping_ext'] ) );
    if( isset($_POST['shipping_int']) && ! empty($_POST['shipping_int']) )
        $order->update_meta_data( 'shipping_int', sanitize_text_field( $_POST['shipping_int'] ) );
    if( !empty( $_POST['_cfdi_catalogue'] ) )
        $order->update_meta_data( '_cfdi_catalogue', sanitize_text_field( $_POST['_cfdi_catalogue'] ) );
}
add_action( 'woocommerce_process_shop_order_meta', 'misha_save_general_details' );
function misha_save_general_details( $ord_id ){
    update_post_meta( $ord_id, '_shipping_rfc_name', wc_clean( $_POST[ '_shipping_rfc_name' ] ) );
    update_post_meta( $ord_id, '_shipping_rfc_field', wc_clean( $_POST[ '_shipping_rfc_field' ] ) );
    update_post_meta( $ord_id, 'shipping_ext', wc_clean( $_POST[ 'shipping_ext' ] ) );
    update_post_meta( $ord_id, 'shipping_ext', wc_clean( $_POST[ 'shipping_int' ] ) );
    update_post_meta( $ord_id, '_cfdi_catalogue', wc_clean( $_POST[ '_cfdi_catalogue' ] ) );
}

add_action( 'woocommerce_before_checkout_shipping_form', 'cfdi_select_field' );
// select
function cfdi_select_field(){
    $checkout = WC()->checkout;
    echo '<div id="cfdi_catalogue">';
    woocommerce_form_field( '_cfdi_catalogue', array(
        'type'          => 'select', // text, textarea, select, radio, checkbox, password, about custom validation a little later
        'required'	=> true, // actually this parameter just adds "*" to the field
        'class'         => array('misha-field', 'form-row-wide','input-group', 'input-container'), // array only, read more about classes and styling in the previous step
        'label'         => 'Catálogo de uso de Comprobantes',
        'label_class'   => 'misha-label', // sometimes you need to customize labels, both string and arrays are supported
        'options'	=> array( // options for <select> or <input type="radio" />
            ''		=> 'Selecciona:', // empty values means that field is not selected
            'Adquisición de mercancias'	=> 'Adquisición de mercancias', // 'value'=>'Name'
            'Devolucioenes y equipo de oficina por inversiones'	=> 'Devolucioenes y equipo de oficina por inversiones',
            'Gastos en general' => 'Gastos en general',
            'Construcciones' => 'Construcciones',
            'Mobiliario y equipo de oficina por inversiones' => 'Mobiliario y equipo de oficina por inversiones',
            'Equipo de transporte' => 'Equipo de transporte',
            'Equipo de computo y accesorios' => 'Equipo de computo y accesorios',
            'Dados, troqueles, moldes, matrices y herramental' => 'Dados, troqueles, moldes, matrices y herramental',
            'Comunicaciones telefónicas' => 'Comunicaciones telefónicas',
            'Comunicaciones satelitales' => 'Comunicaciones satelitales',
            'Otra maquinaria y equipo' => 'Otra maquinaria y equipo',
            'Honorarios médicos, dentales y gastos hospitalarios' => 'Honorarios médicos, dentales y gastos hospitalarios',
            'Gastos médicos por incapacidad o discapacidad' => 'Gastos médicos por incapacidad o discapacidad',
            'Gastos funerales' => 'Gastos funerales',
            'Donativos' => 'Donativos',
            'Intereses reales efectivamente pagados por créditos hipotecarios' => 'Intereses reales efectivamente pagados por créditos hipotecarios',
            'Aportaciones voluntarios al SAR' => 'Aportaciones voluntarios al SAR',
            'Primas por seguros de gastos médicos' => 'Primas por seguros de gastos médicos',
            'Gastos de transportación escolar obligatoria' => 'Gastos de transportación escolar obligatoria',
            'Depósitos en cuentas para el ahorro' => 'Depósitos en cuentas para el ahorro',
            'Pagos por servicios educativos (colegiaturas)' => 'Pagos por servicios educativos (colegiaturas)',
            'Por definir' => 'Por definir',
        )
    ), $checkout->get_value( '_cfdi_catalogue' ) );

    echo '</div>';

    // you can also add some custom HTML here
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'extra_custom_checkout_field_display_admin_order_meta', 10, 1 );
function extra_custom_checkout_field_display_admin_order_meta($order){
    $shipp_rfc_name   = get_post_meta( $order->get_id(), '_shipping_rfc_name', true );
    $shipp_rfc_field  = get_post_meta( $order->get_id(), '_shipping_rfc_field', true );
    $shipp_ext        = get_post_meta( $order->get_id(), 'shipping_ext', true );
    $shipp_int        = get_post_meta( $order->get_id(), 'shipping_int', true );
    $catalogue        = get_post_meta( $order->get_id(), '_cfdi_catalogue',true);
    ?>
    <div class="extra-data">
        <p><strong>Número Exterior:</strong> <?php echo $shipp_ext ?></p>
        <p><strong>Número Interior:</strong> <?php echo $shipp_int ?></p>
        <p><strong>Nombre o razón social:</strong> <?php echo $shipp_rfc_name ?></p>
        <p><strong>RFC a facturar:</strong> <?php echo $shipp_rfc_field ?></p>
        <p><strong>Uso de CFDi:</strong> <?php echo $catalogue ?></p>
    </div>
<?php } ?>