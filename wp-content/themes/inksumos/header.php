<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Thing" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <?php wp_head(); ?>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
    <?php
        // Inlcude Cover Section
        get_template_part('template-parts/section', 'navigation');
    ?>

    <div class="site-branding-container">
        <?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
    </div>