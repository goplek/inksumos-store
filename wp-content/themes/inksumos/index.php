<?php
    get_header();
?>
    <div class="outer-wrapper">

<?php
    // Inlcude Cover Section
    get_template_part('template-parts/section', 'cover');
    // Include Us Section.
    get_template_part('template-parts/section', 'us');
    //Include Services section
    get_template_part('template-parts/section', 'services');
    // Include Inksumos for Business Section.
    get_template_part('template-parts/section', 'business');
    // Include Contact Section.
    get_template_part('template-parts/section', 'contact');

    // Include Footer.
    get_footer();
?>