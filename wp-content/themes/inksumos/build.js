const sass = require('node-sass');

sass.render({
    file: 'style.scss',
    outputStyle: 'compressed',
}, (err, result) => {
    if (err) {
        throw err;
    }

    console.log(result);
});
