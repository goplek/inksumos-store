'use strict';
var contentAllProducts = "", tracing = "", array_filters_selected = [];
var Page = {
    /**
     * @type {jQuery}
     */
    $body: $('body'),
    /**
     * @type {jQuery}
     */
    $window: $(window),
    /**
     * Initialize page scripts.
     */
    init: function () {
        // Initialize page parts.
        var loc = window.location.search;
        if (loc.includes('96')) {
            Page.checkSlugInUrl();
        }
        Page.ChangeViewProducts.init();
        Page.ContactInnerServices.init();
        Page.DetectTypeCategory.init();
        Page.promotionsCarousel.init();
        Page.Contact.init();
        Page.initProfileForm.init();
        Page.deleteProfileModal.init();
        Page.showPromotionsCarousel();
        Page.Accordion.init();
        Page.clickInProduct();
        //Assistant Page
        Page.sliderAssistant.init();
        Page.showTextInstructions();
        Page.DetectRadioSelected();
        Page.clickToRestartAssistantSearch();
        Page.showMorePost();
        Page.ShipmentAddress.init();
        Page.FormFields.init();
        Page.LoginViews.init();

        // Events
        Page.$window.on('load', function () {
            Page._onLoad();
        });
        Page.$window.on('resize', function () {
            Page._onResize();
        });
        Page.$window.on('scroll', function () {
            Page._onScroll();
        });
    },
    /**
     * Fires when the page is loaded.
     * @private
     */
    _onLoad: function () {
        Page.AddressAccordion.init();
    },

    /**
     * Fires when the page is resized.
     * @private
     */
    _onResize: function () {
        Page.promotionsCarousel.init();
        Page.clickInProduct();
    },
    /**
     * Fires on scrolling.
     * @private
     */
    _onScroll: function () {
    },
    /**
     * validate if input content text to search product
     * return {boolean}
     */
    validateBtnSearch: function () {
        var validationSearch = false;
        var contentSearch = document.querySelectorAll('.wrapper-search input[type=text]');
        if (contentSearch) {
            [].forEach.call(contentSearch, function (searchInput) {
                if (searchInput.length === 0) {
                    validationSearch = true;
                }
                return validationSearch;
            });
        }
    },
    /**
     * Get all products by class
     * return {array}
     */
    contentParentProducts: function () {
        var productContents = document.querySelectorAll('.product-products1');
        return productContents;
    },
    /**
     * Get url to detect if contain slug of a product
     */
    checkSlugInUrl: function () {
        var url_loc = window.location.href, separateUrl = '', Utf8ValCorrect = '';
        url_loc.includes("#");
        if (url_loc.includes("#") === true) {
            separateUrl = url_loc.split("#");
            Page.searchSlugInProducts(separateUrl[1]);
        } else {
            separateUrl = url_loc.split('&');
            Utf8ValCorrect = decodeURIComponent(separateUrl[1]);
            Page.showProductsClickHome(Utf8ValCorrect);
        }
    },
    /**
     * clean string of accents
     * @param {string} cadena
     */
    cleanString: function (cadena) {
        var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
        var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
        for (var i = 0; i < acentos.length; i++) {
            cadena = cadena.replace(new RegExp(acentos.charAt(i), 'g'), original.charAt(i));
        }
        cadena = cadena.toLowerCase();
        return cadena;
    },
    /**
     * show Products detect click from home
     * @param {string} data
     */
    showProductsClickHome: function (data) {
        var categories = document.querySelectorAll('.category-product');
        var dataClean = Page.cleanString(data);
        [].forEach.call(categories, function (category) {
            var data_Category = category.getAttribute('data-category');
            if (data_Category === dataClean) {
                setTimeout(function () {
                    category.click();
                }, 10);
            }
        });
    },
    /**
     * Search slug in products to show description cover product
     * @param slug
     */
    searchSlugInProducts: function (slug) {
        var productsToCheck = Page.contentParentProducts(), productSlug = '';

        [].forEach.call(productsToCheck, function (productCheck) {
            productSlug = productCheck.getAttribute('data-slug');
            if (productSlug === slug) {
                setTimeout(function () {
                    productCheck.click();
                }, 20);
            }
        });
    },
    /**
     * Click in btn see more products
     */
    showMorePost: function () {
        var seeMoreProducts = document.querySelector('.see-more-inner-products');
        if (seeMoreProducts) {
            seeMoreProducts.addEventListener('click', function (ev) {
                var categorySelected = document.querySelector('.category-product.select-product');
                var allProducts = document.querySelectorAll('.product-products1.d-none');
                var allProductsLenght = allProducts.length;
                var ProductsToShow = 4;
                ev.preventDefault();
                if (allProductsLenght > 0) {
                    [].forEach.call(allProducts, function (productTo, index) {
                        if ((index + 1) <= ProductsToShow) {
                            if (categorySelected != null) {
                                var categoryProduct = productTo.getAttribute('data-category');
                                var categorySelectedAttr = categorySelected.getAttribute('data-selected');
                                if (categoryProduct === categorySelectedAttr) {
                                    pl.Classie.removeClass(productTo, 'd-none');
                                }
                            } else {
                                pl.Classie.removeClass(productTo, 'd-none');
                            }
                        }
                    });
                } else {
                    alert('No hay más productos para mostrar');
                }
            });
        }
    },
    /**
     * Detect click en product to open te modal
     */
    clickInProduct: function () {
        contentAllProducts = Page.contentParentProducts();
        [].forEach.call(contentAllProducts, function (product) {
            product.addEventListener('click', function (ev) {
                ev.preventDefault();
                Page.openModal(product);
            });
        });
    },
    /**
     * Get elements of product
     */
    sectionInnerModal: function (product) {
        var catProduct = product.getAttribute('data-category'); // get data to compare  products type to suggestions
        var idProduct = product.getAttribute('data-id'); // get data id to compare product with suggestions
        tracing = document.createElement('div');
        var carrouselWrapper = document.createElement('div');
        var imagesLinks = "";
        carrouselWrapper.className += 'carousel-images-modal';
        tracing.className += 'modal-product-information';
        var productImage = product.querySelector('.image-product-products img').getAttribute('srcset');
        var srcProduct = productImage.split(' ', [1]);
        var galleryViewsProduct = product.querySelectorAll('.view-product-gallery img');
        var productTitle = product.querySelector('.card-body-title').textContent;
        var productSku = product.querySelector('.card-body-sku').textContent;
        var productDescriptionComplete = product.querySelector('.description-complete').textContent;
        //var productDescriptionShort = product.querySelector('p .s1').textContent;
        var productDescriptionShort = product.querySelector('p').textContent;
        var productPrice = product.querySelector('.about-product-price').textContent;
        var productCarImage = product.querySelector('.wrapper-car-price .white-cart').getAttribute('src');
        var addToCart = product.querySelector('.inner-about-product').getAttribute('data-link');

        [].forEach.call(galleryViewsProduct, function (image) {
            imagesLinks += `
                <div class="thumb">
                    <a class="inner-thumb" href="javascript:void(0)">
                        <div class="content-img-carousel">
                            <img class="img-fluid" src="${image.src}">
                        </div>
                    </a>
                </div>
            `;
        });
        imagesLinks = `
            <div class="inner-carousel-images">
                <div class="util-carrousel changeContent">${ imagesLinks }</div>
            </div>
        `;

        /**
         * Suggestion carrousel
         */
        var suggestionCarrousel = "";

        [].forEach.call(contentAllProducts, function (suggestion) {
            var catSuggestion = suggestion.getAttribute('data-category');
            var idSuggestion = suggestion.getAttribute('data-id');
            if ((catSuggestion === catProduct) && (idSuggestion != idProduct)) {

                var image_suggestion = suggestion.querySelector('.image-product-products img').getAttribute('srcset');
                var srcProduct_suggestion = image_suggestion.split(' ', [1]);
                var title_suggestion = suggestion.querySelector('.card-body-title').textContent;
                var sku_suggestion = suggestion.querySelector('.card-body-sku').textContent;
                var gallery_suggestion = suggestion.querySelectorAll('.view-product-gallery img');
                var gallery_suggestion_links = "";
                var description_complete_suggestion = suggestion.querySelector('.description-complete').textContent;
                var description_short_suggestion = suggestion.querySelector('.short-description p').textContent;
                var price_suggestion = suggestion.querySelector('.about-product-price').textContent;
                var addCar = suggestion.querySelector('.inner-about-product').getAttribute('data-link');
                var image_cart_suggestion = suggestion.querySelector('.wrapper-car-price .img').getAttribute('src');

                [].forEach.call(gallery_suggestion, function (image) {
                    gallery_suggestion_links += `<img class="img-fluid" src="${ image.src }">`;
                });

                suggestionCarrousel += `<div class="thumb">
                     <a class="product-products-modal" href="javascript:void(0)" data-category="${ catSuggestion }">
                        <div class="card card-profile mdl-product">
                            <div class="card-header mdl-header-image">
                                <div class="mdl-image-product image-product-products">
                                    <img src="${ srcProduct_suggestion }" srcset="${ srcProduct_suggestion }">
                                </div>
                            </div>
                            <div class="card-body mdl-body-text ">
                                <div class="mdl-list-description">
                                    <h4 class="card-title mdl-body-title  card-body-title">${ title_suggestion }</h4>
                                    <p class="card-text mdl-body-sku card-body-sku">${ sku_suggestion }</p>
                                </div>
                                <div class="view-product-gallery d-none">
                                  ${ gallery_suggestion_links }
                                </div>
                                <div class="description-complete d-none">
                                    ${ description_complete_suggestion }
                                </div>
                                <p class="card-text mdl-text-body">
                                    <span class="s1">
                                        ${ description_short_suggestion }
                                    </span>
                                </p>
                                <div class="mdl-wrapper-product inner-about-product" data-link="${ addCar }">
                                    <p class="card-text mdl-product-price about-product-price">${ price_suggestion }</p>
                                    <div class="mdl-inner-product">
                                        <p class="card-text mdl-product-available">
                                            Disponible</p>
                                        <div class="mdl-car-price wrapper-car-price">
                                            <img class="white-cart" src="${ image_cart_suggestion }">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> `;
            }
        });
        suggestionCarrousel = `
                    <div class="inner-suggestions">
                        <div class="util-carrousel changeContent">${ suggestionCarrousel }</div>
                    </div>
                `;

        var Product = `<div class="wrapper-modal-product">
                            <div class="pl-modal-close-button inner-btn"></div>
                            <div class="wrapper-image-product">
                                <div class="product-image">
                                    <div class="inner-product-image">
                                        <img src="${ srcProduct }">
                                    </div>
                                </div>
                                ${ imagesLinks }
                            </div>
                            <div class="wrapper-information-product">
                                <div class="inner-wrapper-information">
                                    <div class="title-product">
                                        ${ productTitle }
                                    </div>
                                    <div class="sku-product">
                                        ${ productSku }
                                    </div>
                                    <div class="price-product">
                                       ${ productPrice }
                                    </div>
                                    <div class="vat-product">
                                        *Los precios incluyen I.V.A.
                                    </div>
                                    <div class="description-product">
                                        ${ productDescriptionComplete }
                                    </div>
                                    <a class="addCart" href="${addToCart}">
                                        <div class="inneraddCart">
                                            <div class="text-cart">Agregar</div>
                                            <div class="img-cart">
                                                <img class="img-fluid" src="${ productCarImage }"
                                                     alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                       <div class="wrapper-suggestions-modal">
                            <div class="title-suggestion-modal">
                                También te podrían interesar los siguientes productos:
                            </div>
                            <div class="content-suggestions-modal">
                                <div class="carrousel-suggestions">
                                    ${ suggestionCarrousel }
                                </div>
                            </div>
                        </div>`;
        tracing.innerHTML = Product;
        return tracing;
    },
    /**
     * Initialize carrousel in modal
     */
    initializeCarrousel: function () {
        var sectionToInitializeViewGallery = tracing.querySelector('.wrapper-image-product .inner-carousel-images .util-carrousel');
        var sectionToInitializeSuggestion = tracing.querySelector('.wrapper-suggestions-modal .content-suggestions-modal .inner-suggestions .util-carrousel');

        sectionToInitializeViewGallery.addEventListener(pl.Modal.transitionSelect(), function (ev) {
            ev.stopPropagation();
        });
        sectionToInitializeSuggestion.addEventListener(pl.Modal.transitionSelect(), function (ev) {
            ev.stopPropagation();
        })

        pl.Classie.removeClass(sectionToInitializeViewGallery, 'changeContent');
        pl.Classie.removeClass(sectionToInitializeSuggestion, 'changeContent');

        var $carruselViewsGallery = $(sectionToInitializeViewGallery);
        var thumb_show = Page.isMobileSize() ? 1 : 3;
        var $carruselSuggestions = $(sectionToInitializeSuggestion);
        var thumb_show_suggestion = Page.isMobileSize() ? 1 : 4;

        var carrousel = new util.Carrousel($carruselViewsGallery, {
            autoPlay: false,
            delay: 2000,
            infiniteScroll: true,
            thumbsToDisplay: thumb_show,
            scaleImages: true,
            scrollSpeed: 350
        }, function (c) {
            //Swipe handler for slider
            $carruselViewsGallery.on("swiperight", function () {
                c.prevButton.trigger('click');
            });
            $carruselViewsGallery.on("swipeleft", function () {
                c.nextButton.trigger('click');
            });
        });

        var carrousel1 = new util.Carrousel($carruselSuggestions, {
            autoPlay: false,
            delay: 2000,
            infiniteScroll: true,
            thumbsToDisplay: thumb_show_suggestion,
            scaleImages: true,
            scrollSpeed: 350
        }, function (c) {
            //Swipe handler for slider
            $carruselSuggestions.on("swiperight", function () {
                c.prevButton.trigger('click');
            });
            $carruselSuggestions.on("swipeleft", function () {
                c.nextButton.trigger('click');
            });
        });
    },
    /**
     * Add event buttton to close modal
     * @param{InstanceType} modal
     */
    clickCloseButton: function (modal) {
        var close_btn_clone = tracing.querySelector('.inner-btn');
        close_btn_clone.addEventListener('click', function () {
            modal.close();
        });
    },
    /**
     * Click products in modal
     * @param{InstanceType} modal
     */
    clickProductsModal: function (modal) {
        var suggestion_products = tracing.querySelectorAll('.product-products-modal');
        [].forEach.call(suggestion_products, function (suggestion_product) {
            suggestion_product.addEventListener('click', function (ev) {
                ev.preventDefault();
                modal.setContent(
                    Page.sectionInnerModal(suggestion_product, contentAllProducts),
                    Page.clickCloseButton(modal),
                    Page.clickProductsModal(modal),
                    setTimeout(function () {
                        Page.initializeCarrousel();
                    }, 200)
                );
            });
        });
    },
    /**
     * Open modal
     */
    openModal: function (product) {
        tracing = Page.sectionInnerModal(product, contentAllProducts); //template

        // Create modal instance.
        var modal = new pl.Modal({effectName: 'pl-effect-11'});

        // Show modal on click.
        modal.open(tracing);
        modal.opened.add(function () {
            Page.clickCloseButton(modal);
            Page.clickProductsModal(modal);
            Page.initializeCarrousel();
        });
    },
    /**
     * Effect accordion to filters
     */
    Accordion: {
        /**
         * @type jQuery
         */
        $section: null,
        /**
         * @type jQuery
         */
        $clickElement: null,
        /**
         * Initialize function
         */
        init: function () {
            var section = document.querySelectorAll('.filters');
            [].forEach.call(section, function (ele) {
                var currentElement = ele.querySelector('.inner-filter');
                currentElement.addEventListener('click', function () {
                    Page.Accordion.clickAction(currentElement);
                });
            });
        },
        /**
         * Reset all status
         * Patch in order to close all elements on click
         */
        resetStatus: function (ele) {
            var heiContainer = ele.querySelector('.content-filter');
            pl.Classie.removeClass(ele, 'open');
            heiContainer.style.height = '0px';
        },
        /**
         * Get element that was Clicked
         * @param {HTMLElement} ele
         */
        clickAction: function (ele) {
            var parentElement = ele.parentElement;
            if (parentElement.classList.contains('open')) {
                Page.Accordion.resetStatus(parentElement);
            } else {
                this.changeClass(parentElement);
            }
        },
        /**
         * Add class  Open
         * @param {HTMLElement} parentElement
         */
        changeClass: function (parentElement) {
            var parentEle = parentElement;
            pl.Classie.addClass(parentEle, 'open');
            // Page.Accordion.checkCheckBox(parentEle);
            this.changeHeight(parentEle);
        },
        /**
         * Change height of element Content
         * @param {HTMLElement} parentEle
         */
        changeHeight: function (parentEle) {
            var contentWrapper = parentEle.querySelector('.content-filter');
            contentWrapper.style.height = this.getHeight(parentEle) + 'px';
        },
        /**
         * Return height to be assigned to clicked element
         * @param {HTMLElement} option
         * @returns {*}
         */
        getHeight: function (option) {
            var contentInformation = option.querySelector('.inner-container .content-features');
            var newHeight = contentInformation.offsetHeight;
            return newHeight
        },
    },
    /**
     * Detect if click has been given in div to view of products
     */
    ChangeViewProducts: {
        init: function () {
            var ElementsTypeNavigation = document.querySelectorAll('.type-view');
            var viewContentElements = document.querySelector('.body-content-products');
            [].forEach.call(ElementsTypeNavigation, function (elementypenav) {
                elementypenav.addEventListener('click', function () {
                    Page.ChangeViewProducts.resetStatus(ElementsTypeNavigation);
                    Page.ChangeViewProducts.resetContentView(viewContentElements);
                    pl.Classie.addClass(elementypenav, 'active');
                });
            });
        },
        /**
         * Reset status of type view element
         * @param {HTMLElement}  section
         */
        resetStatus: function (section) {
            [].forEach.call(section, function (ele) {
                pl.Classie.removeClass(ele, 'active');
            });
        },
        /**
         * Reset status of type to view content
         * @param {HTMLElement}  section
         */
        resetContentView: function (viewContent) {
            if (viewContent.classList.contains('list-products')) {
                pl.Classie.removeClass(viewContent, 'list-products');
            } else {
                pl.Classie.addClass(viewContent, 'list-products');
            }
        },
    },
    /**
     * Detect if click has been given in div to view of products
     * and multiple functions
     */
    DetectTypeCategory: {
        init: function () {
            var ElementCategoryType = document.querySelectorAll('.category-product'); //content two principal categories
            var productTypes = document.querySelectorAll('.product-products1'); //content all products that are in view
            var categorySelect = document.querySelector('.categorySelect'); //content the value when a element of category is selected
            [].forEach.call(ElementCategoryType, function (elementypecat) {
                elementypecat.addEventListener('click', function () {
                    var textData = elementypecat.getAttribute('data-category'); // content the category selected
                    var idData = elementypecat.getAttribute('data-selected');  // content value of category selected
                    var contentFilters = document.querySelector('.wrapper-content-filters'); // content section with filter to products
                    var textTitle = document.querySelector('.title-main-products span'); // content the title of category selected
                    textTitle.innerText = textData; // add the category name select
                    Page.DetectTypeCategory.optionFilterShow(textData); // show or hide filters depending of category selected
                    Page.DetectTypeCategory.resetStatus(ElementCategoryType, 'select-product'); // reset filters selected by change of click in category
                    categorySelect.innerText = textData; // change category when
                    pl.Classie.addClass(elementypecat, 'select-product');  // add class to change list of filters in view
                    Page.DetectTypeCategory.separateProducts(idData, productTypes); // show or hide products that are in view depending of category
                    if (contentFilters.classList.contains('initializeSection')) {
                        pl.Classie.removeClass(contentFilters, 'initializeSection');
                    }
                });
            });
        },
        /**
         * Reset status of type view element
         * @param {HTMLElement}  section
         * @class
         */
        resetStatus: function (section, class_select) {
            [].forEach.call(section, function (ele) {
                pl.Classie.removeClass(ele, class_select);
            });
        },
        /**
         * detect elements that belong a some category
         *@param{HTMLElement} section
         * @param{post_id} idData
         */
        separateProducts: function (idData, productTypes) {
            var productsArraySelected = [];
            Page.DetectTypeCategory.resetStatus(productTypes, 'separateByCat');
            var showMoreProducts = document.querySelector('.see-more-inner-products');
            [].forEach.call(productTypes, function (productType) {
                var data_id = productType.getAttribute('data-category');
                if (data_id != idData) {
                    if (!(productType.classList.contains('d-none'))) {
                        pl.Classie.removeClass(productType, 'productsFiltered');
                        pl.Classie.addClass(productType, 'separateByCat');
                    }
                } else {
                    if (productType.classList.contains('d-none')) {
                        showMoreProducts.click();
                    } else {
                        productsArraySelected.push(productType);
                        pl.Classie.addClass(productType, 'productsFiltered');
                    }
                }
            });
            Page.DetectTypeCategory.AttributesProducts(productsArraySelected);
            Page.ClickInFilters();
        },
        /**
         *Show o hide option filter side
         * @param {string} textData
         */
        optionFilterShow: function (textData) {
            var OptionsFilter = document.querySelectorAll('.filters');
            [].forEach.call(OptionsFilter, function (optionFilter) {
                var OptioDataSelected = optionFilter.getAttribute('data-selected');
                if ((textData === 'cartuchos' & OptioDataSelected === "2") || (textData === 'cartuchos' & OptioDataSelected === "5") || (textData === 'cartuchos' & OptioDataSelected === "3")) {
                    pl.Classie.addClass(optionFilter, 'd-none');
                } else if ((textData === 'impresiones' & OptioDataSelected === "6") || (textData === 'impresiones' & OptioDataSelected === "7")) {
                    pl.Classie.addClass(optionFilter, 'd-none');
                } else if (OptioDataSelected === "8") {
                    pl.Classie.addClass(optionFilter, 'd-none');
                } else {
                    pl.Classie.removeClass(optionFilter, 'd-none');
                }
            });
        },
        /**
         * Detect filters to show depending of the category selected
         * get all  attributes of products
         * @type {NodeListOf<Element>}
         */
        AttributesProducts: function (productsArraySelected) {
            var product_attributes = '', array_values_attributes = [];
            [].forEach.call(productsArraySelected, function (product) {
                product_attributes += product.getAttribute('data-attributes') + ',';
            });
            array_values_attributes = product_attributes.split(",");
            Page.DetectTypeCategory.attributesFilters(array_values_attributes);
        },
        /**
         * get all attributes of filter to compare with attributes in products
         * @type {NodeListOf<Element>}
         */
        attributesFilters: function (array_values_attributes) {
            var filters_date = document.querySelectorAll('.filters label'), validation_Element = '',
                array_filters_selected = [];
            Page.DetectTypeCategory.resetStatus(filters_date, 'd-none');
            [].forEach.call(filters_date, function (filter) {
                validation_Element = array_values_attributes.includes(filter.innerText);
                if (!(validation_Element)) {
                    pl.Classie.addClass(filter, 'd-none');
                } else {
                    array_filters_selected.push(filter);
                }
            });
        },
    },

    /**
     * Detect click in filters to apply in products
     */
    ClickInFilters: function () {
        var allFilterToSelect = document.querySelectorAll('.content-features label'), multiple_Filter = [], index = 0;

        if (allFilterToSelect) {
            [].forEach.call(allFilterToSelect, function (filterToSelect) {
                filterToSelect.addEventListener('change', function (ev) {
                    if (ev.target.checked) {
                        multiple_Filter.push(ev.target.id);
                    } else {
                        const index = multiple_Filter.indexOf(ev.target.id);
                        multiple_Filter.splice(index, 1);
                    }
                    Page.FilteredProductsByTag(multiple_Filter);
                });
            });
        }
    },
    /**
     * Filter products depending of filter selected
     * @param {string} filter_content
     */
    FilteredProductsByTag: function (multiple_Filter) {
        const products_filtered = document.querySelectorAll('.productsFiltered');

        [].forEach.call(products_filtered, product => {
            if (pl.Classie.hasClass(product, 'd-none')) {
                pl.Classie.removeClass(product, 'd-none');
            }
        });

        if (multiple_Filter.length > 0) {
            [].forEach.call(products_filtered, product => {
                let hasClass = false;
                [].forEach.call(multiple_Filter, filter => {
                    if (pl.Classie.hasClass(product, filter)) {
                        hasClass = true;
                    }
                });

                if (!hasClass) {
                    pl.Classie.addClass(product, 'd-none');
                }
            });
        }
    },
    /**
     * Detect click in radio button
     * Get attribute value to assistant search
     */
    DetectRadioSelected: function () {
        var allRadios = document.querySelectorAll('.form-check-label');
        var next_slide_btn = document.querySelector('.wrapper-slider-assistant .next-text');

        if (next_slide_btn) {
            [].forEach.call(allRadios, function (rad) {
                rad.addEventListener('click', function () {
                    var inputValue = document.querySelector('.slide .inner-assistant-thumb .value-options');
                    var radiochild = rad.querySelector('.form-check-input');
                    var Childvalue = radiochild.getAttribute('value');
                    inputValue.innerText = Childvalue;
                    pl.Classie.removeClass(next_slide_btn, 'notAvailable');
                });
            });
            pl.Classie.addClass(next_slide_btn, 'notAvailable');
        }
    },
    /**
     * Compare attributes of product with tags of assistant search array
     * @param {Array} tagStringToArray
     * @param {Array} attributesProductArray
     * @return {boolean}
     */
    compareTagsFilterAssistant: function (tagStringToArray, attributesProductArray) {
        var equal = 0;
        for (var i = 0; i < attributesProductArray.length; i++) {
            for (var j = 0; j < tagStringToArray.length; j++) {
                if (attributesProductArray[i] === tagStringToArray[j]) {
                    equal++;
                }
            }
        }
        if (equal === tagStringToArray.length) {
            return true;
        }
    },
    /**
     * Get filter of assistant search to search similar products
     * and add class to show or hide product.
     * @param {string} tagsToString
     */
    filterAssistantProducts: function (tagsToString) {
        var allProductsInview = Page.contentParentProducts();
        var tagStringToArray = tagsToString.split(",");

        [].forEach.call(allProductsInview, function (productInview) {
            var attributesProduct = productInview.getAttribute('data-attributes');
            var attributesProductArray = attributesProduct.split(",");
            if (Page.compareTagsFilterAssistant(tagStringToArray, attributesProductArray) != true) {
                pl.Classie.addClass(productInview, 'd-none');
            }
        });
    },
    /**
     * Return to principal main to select assistant search
     */
    clickToRestartAssistantSearch: function () {
        var WrapperSlider = document.querySelector('.wrapper-slider-assistant');
        var assistantResult = document.querySelector('.assistant-result');

        var btnReturnAssistant = document.querySelector('.return-initialize-assistant');
        if (btnReturnAssistant) {
            btnReturnAssistant.addEventListener('click', function () {
                Page.changeRemoveClassInElement(WrapperSlider, 'd-none');
                Page.changeAddClassInElement(assistantResult, 'd-none');
            });
        }
    },
    /**
     @param {HTMLElement} elementToChangeClass
     * @param {class} nameClass
     */
    changeAddClassInElement: function (elementToChangeClass, nameClass) {
        pl.Classie.addClass(elementToChangeClass, nameClass);
    },
    /**
     * @param {HTMLElement} elementToChangeClass
     * @param {class} Nameclass
     */
    changeRemoveClassInElement: function (elementToChangeClass, Nameclass) {
        pl.Classie.removeClass(elementToChangeClass, Nameclass);
    },
    /**
     * Detect click in btn search to concat all tags
     * to search products with those tags
     * @param {Array} tagAssistantToConcat
     */
    ClickSearchElement: function (tagAssistantToConcat) {
        var sectionWrapperSlider = document.querySelector('.wrapper-slider-assistant');
        var assistantResult = document.querySelector('.assistant-result');
        var searchELement = document.querySelector('.wrapper-slider-assistant .search-element'), joinTags = [],
            tagsToString = '', priceTag = '';
        searchELement.addEventListener('click', function () {
            var next_text_btn = document.querySelector('.next-text');
            var search_text_btn = document.querySelector('.wrapper-slider-assistant .search-element');
            var quantityToProducts = document.querySelector('.wrapper-slider-assistant .slider.assistant .slide .min-value');
            var priceInt = parseInt(quantityToProducts.innerText.replace(/[^0-9]/g, ''));

            if (priceInt > 0 && priceInt <= 1000) {
                priceTag = "0-1000";
            } else if (priceInt > 1000 && priceInt <= 2000) {
                priceTag = "1001-2000";
            } else if (priceInt > 2000 && priceInt <= 3000) {
                priceTag = "2001-3000";
            } else if (priceInt > 3000 && priceInt <= 4000) {
                priceTag = "3001-4000";
            } else if (priceInt > 4000 && priceInt <= 5000) {
                priceTag = "4001-5000";
            } else if (priceInt > 5000 && priceInt <= 6000) {
                priceTag = "5001-6000";
            } else if (priceInt > 6000 && priceInt <= 7000) {
                priceTag = "6001-7000";
            } else if (priceInt > 7000 && priceInt <= 8000) {
                priceTag = "7001-8000";
            } else if (priceInt > 8000 && priceInt <= 9000) {
                priceTag = "8001-9000";
            } else if (priceInt > 9000 && priceInt <= 10000) {
                priceTag = "9001-10000";
            }

            joinTags = tagAssistantToConcat.concat(priceTag);
            tagsToString = joinTags.toString();
            Page.changeAddClassInElement(sectionWrapperSlider, 'd-none');
            Page.filterAssistantProducts(tagsToString);
            Page.changeRemoveClassInElement(assistantResult, 'd-none');
            next_text_btn.click();
            Page.changeRemoveClassInElement(next_text_btn, 'd-none');
            Page.changeAddClassInElement(search_text_btn, 'd-none');
        });
    },
    /**
     * Show or hide text btn in slider
     */
    showTextInstructions: function () {
        var directionSlide = document.querySelectorAll('.wrapper-slider-assistant .directions');
        var prev_button = document.querySelector('.wrapper-slider-assistant .prev-text');
        var next_button = document.querySelector('.wrapper-slider-assistant .next-text');
        var search_button = document.querySelector('.wrapper-slider-assistant .search-element');
        var all_dots = document.querySelectorAll('.wrapper-slider-assistant .slider.assistant .dots .dot');
        var dots_length = all_dots.length, arrayTagsAssistant = [], tagAssistantToConcat = [];

        [].forEach.call(directionSlide, function (direction_Slide) {
            direction_Slide.addEventListener('click', function () {
                var InputValueElement = document.querySelector('.wrapper-slider-assistant .slider.assistant .slide .value-options');
                if (direction_Slide.classList.contains('next-text')) {
                    Page.DetectRadioSelected();
                    arrayTagsAssistant.push(InputValueElement.innerText); //Add element to array to search elements to show
                    InputValueElement.innerText = '';   //clean the variable to change value
                    pl.Classie.removeClass(prev_button, 'active');
                    pl.Classie.addClass(next_button, 'active');
                } else {
                    pl.Classie.removeClass(next_button, 'active');
                    pl.Classie.addClass(prev_button, 'active');
                    pl.Classie.addClass(direction_Slide, 'notAvailable');
                }

                [].forEach.call(all_dots, function (dot, index) {
                    if (dot.classList.contains('active')) {
                        if (index === (dots_length - 1)) {
                            pl.Classie.addClass(next_button, 'd-none');
                            pl.Classie.removeClass(search_button, 'd-none');
                            Page.ClickSearchElement(arrayTagsAssistant);
                        } else if (index === 0) {
                            pl.Classie.addClass(prev_button, 'd-none');
                            pl.Classie.removeClass(prev_button, 'active');
                        } else {
                            pl.Classie.addClass(search_button, 'd-none');
                            pl.Classie.removeClass(next_button, 'd-none');
                            pl.Classie.removeClass(prev_button, 'd-none');
                        }
                    }
                });
            });
        });
    },
    /**
     * Detect if current viewport has mobile size
     * @returns {boolean}
     */
    isMobileSize: function () {
        return Page.$window.width() < 768;
    },
    /**
     * Show section carousel after load images
     */
    showPromotionsCarousel: function () {
        var elementCarousel = document.querySelector('.inner-promotions-carousel');
        if (elementCarousel) {
            setTimeout(function () {
                pl.Classie.removeClass(elementCarousel, 'hide-moment');
            }, 1500);

        }
    },
    /**
     * Create an slider.
     * @param {string} selector
     * @param {Object} settings
     * @param {function} callback
     */
    createSlider: function (selector, settings, callback) {
        var slider = $(selector);

        if (slider.length)
            slider.slider(settings, callback);
    },
    /**
     * Slider section
     */
    sliderAssistant: {
        /**
         * Initialize sliders.
         */
        init: function () {
            var slide = $('.slider.assistant');

            Page.Calculator.init();

            var sliderInstance = Page.createSlider(slide, {
                    autoPlay: false,
                    activeKeyControls: false,
                    aspectRatio: {h: 320, w: 885},
                    //calcAspectRatio: calcAsp,
                    displayTime: 7000,
                    searchFor: '.slide',
                    showThumbnails: false,
                    showArrows: true,
                    showLabels: false,
                    showDots: true,
                    showLoadingBar: false,
                    transitionTime: 500
                }, function (s) {

                    const next = $('.next-text');
                    const prev = $('.prev-text');

                    next.on('click', function (ev) {
                        s.isNext('right');
                    });
                    prev.on('click', function (ev) {
                        s.isNext('left');
                    });
                    s.element.on('after-slide-change', (e, current) => {
                        if (current.data('calculator')) {
                            Page.Calculator.resizeRs();
                        }
                    });

                    slide.data('instance', s);
                }
            );
        },
    },
    /**
     * Calculator
     */
    Calculator: {

        /**
         * @type HTMLElement|Node
         */
        elem: null,

        /**
         * Initialize calculator section.
         */
        init: function () {
            this.elem = document.querySelector('.calculator');

            if (this.elem) {
                let rangeSlider = this.elem.querySelector('.range-slider');
                this.costInRealTime = this.elem.querySelector('.min-value');

                // Create range slider.
                let rs = new pl.RangeSlider(rangeSlider, {
                    minValue: 0,
                    maxValue: 10000
                });

                this.rs = rs;
                rs.updating.add(this.handleRSUpdating.bind(this));
            }
        },

        resizeRs: function () {
            this.rs.calcSizes();
        },
        /**
         * Handles range slider updating event.
         * @param {number} percentage
         * @param {number} value
         */
        handleRSUpdating: function (percentage, value) {
            this.costInRealTime.innerText = `$${this.numberWithCommas(Math.round(value))}`;
        },
        /**
         * Format number to thousands.
         * @param x
         * @returns {string}
         */
        numberWithCommas: function (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    },
    /**
     * Promotions Carousel
     */
    promotionsCarousel: {
        /**
         * @type jQuery
         */
        $elem: null,
        /**
         * Initialize page part.
         */
        init: function () {
            var $carrousel = $(".inner-promotions-carousel .inner-carrousel .util-carrousel");
            var thumb_show = Page.isMobileSize() ? 1 : 4;
            var infinite_validation = Page.isMobileSize() ? true : false;

            var carrousel = new util.Carrousel($carrousel, {
                autoPlay: false,
                delay: 2000,
                infiniteScroll: infinite_validation,
                thumbsToDisplay: thumb_show,
                scaleImages: true,
                scrollSpeed: 350
            }, function (c) {
                //Swipe handler for slider
                $carrousel.on("swiperight", function () {
                    c.prevButton.trigger('click');
                });
                $carrousel.on("swipeleft", function () {
                    c.nextButton.trigger('click');
                });
            });
        },
    },
    /**
     * Contact form
     */
    Contact: {
        /**
         * @type jQuery
         */
        $elem: null,

        /**
         * Initialize page part.
         */
        init: function () {
            this.$elemLogIn = $('.woocommerce-form-login');
            this.$elemSignUp = $('.woocommerce-form-register');
            this.$elemLostPwd = $('.woocommerce-form-lost-psswd');
            this.$elemCheckout = $('.woocommerce-form-checkout');
            this.$elemCheck = $('.woocommerce-form-with-checkout');

            if (this.$elemLogIn.length && this.$elemSignUp.length) {
                this.initForm();
            }

            else if (this.$elemLostPwd.length) {
                this.secondInitForm();
            }

            else if (this.$elemCheckout.length) {
                this.thirdInitForm();
            }

            else if (this.$elemCheck.length) {
                this.fourthInitForm();
            }
        },

        /**
         * Initialize form.
         */
        initForm: function () {

            // FORM LOGIN
            let formEl = this.$elemLogIn.get(0).querySelector('.form-login');
            let tecform = new pl.ContactForm(formEl, {
                url: '/fragment/themes/inksumos/process-ajax.php',
                useAjax: false,
                inputSelectors: [
                    "input[type=text]",
                    "input[type=email]",
                    "input[type=password]",
                    "input[type=checkbox]",
                    "textarea"
                ]
            });

            // Assign events.
            tecform.error.add((status, statusText) => {
                this.handleError(tecform, status, statusText)
            });
            tecform.sending.add(() => {
                this.handleSending(tecform)
            });
            tecform.success.add((response, status, statusText) => {
                this.handleSuccess(tecform, response, status, statusText)
            });
            // FORM LOGIN

            // FORM SIGNUP
            let formSignup = this.$elemSignUp.get(0).querySelector('.form-signup');
            let signupForm = new pl.ContactForm(formSignup, {
                url: '/fragment/themes/inksumos/process-ajax.php',
                useAjax: false,
                inputSelectors: [
                    "input[type=text]",
                    "input[type=email]",
                    "input[type=password]",
                    "input[type=checkbox]",
                    "textarea"
                ]
            });
        },

        secondInitForm: function () {
            // FORM LOST PASSWORD
            let formLost = this.$elemLostPwd.get(0).querySelector('.form-lost-password');
            let LostForm = new pl.ContactForm(formLost, {
                url: '/fragment/themes/inksumos/process-ajax.php',
                useAjax: false,
                inputSelectors: [
                    "input[type=text]",
                    "input[type=email]",
                    "input[type=password]",
                    "input[type=checkbox]",
                    "textarea"
                ]
            });
            // FORM LOST PASSWORD
        },

        thirdInitForm: function () {

            //FORM WITHOUT LOGIN
            let formCheckout = this.$elemCheckout.get(0).querySelector('.checkout-logout');
            let checkoutForm = new pl.ContactForm(formCheckout, {
                url: '/fragment/themes/inksumos/process-ajax.php',
                useAjax: false,
                inputSelectors: [
                    "input[type=text]",
                    "input[type=email]",
                    "input[type=password]",
                    "input[name=Terminos]",
                    "textarea"
                ]
            });
            //FORM WITHOUT LOGIN
        },

        fourthInitForm: function () {

            //FORM WITHOUT LOGIN
            let formCheckout = this.$elemCheck.get(0).querySelector('.login-checkout');
            let checkoutForm = new pl.ContactForm(formCheckout, {
                url: '/fragment/themes/inksumos/process-ajax.php',
                useAjax: false,
                inputSelectors: [
                    "input[type=text]",
                    "input[type=email]",
                    "input[type=password]",
                    "input[name=Terminos]",
                    "input[name=select-address]",
                    "textarea"
                ]
            });
            //FORM WITHOUT LOGIN
        },

        /**
         * Create HTML Element
         * @returns {HTMLElement}
         */
        createElement: function () {
            if (!this.formMessage) {
                this.formMessage = document.createElement('div');
                this.formMessage.className += 'form-message';
            }
            return this.formMessage;
        },

        /**
         * Fires if message fails.
         * @param {HTMLElement} form
         * @param {string} status
         * @param {string} statusText
         */
        onError: function (status, statusText, form) {

            let statusMessage = form.querySelector('.status-message');
            statusMessage.classList.remove('hidden');
            statusMessage.innerHTML = '<div class="error">Hubo un error al enviar tu mensaje, por favor intentalo más tarde.</div>';
            Page.gaCount('Formulario', 'Formulario Error');

            this.removeFormStatus(form);
        },
        /**
         * Handle input error event.
         * @param {HTMLInputElement} input
         */
        handleInputError: function (input) {
            let clue = input.dataset['clue'];

            if (clue) {
                input.setAttribute('placeholder', clue);
            }
        },

        /**
         * Handle form success event.
         * @param {pl.ContactForm} form
         * @param {object} response
         * @param {string} status
         * @param {string} statusText
         */
        handleSuccess: function (form, response, status, statusText) {
            let formEl = form.element;
            let messageEl = Page.Contact.createElement();

            if (!!parseInt(response)) {
                let inputs = form.texts;

                // Return original placeholder.
                [].forEach.call(inputs, input => {
                    input.setAttribute('placeholder', input.name);
                });

                // Show success message.
                this.setFormState("success", formEl);
                messageEl.innerHTML = '<div class="contact-msg">' +
                    '<div class="loader-container success">' +
                    '<div class="done"></div>' +
                    '</div>' +
                    '<div class="text success">' +
                    '<div class="icon"><img src="fragment/themes/inksumos/design/imgs/loader-02.svg"></div>' +
                    '<div class="row1">Tu mensaje se ha enviado.</div>' +
                    '</div>';

                // Remove message.
                this.removeFormMessage(form);

            } else {
                this.handleError(form, status, statusText);
            }


        },

        /**
         * Handle form sending event.
         * @param {pl.ContactForm} form
         */
        handleSending: function (form) {
            let formEl = form.element;
            let body = $('body');
            let msg = $('.block.contact .intro');
            var c = $('.block.contact.contact-form');
            body.addClass('no-scroll');
            msg.fadeOut(10);

            let messageEl = Page.Contact.createElement();
            messageEl.innerHTML =
                '<div class="contact-msg">' +
                '<div class="loader-container">' +
                '<div class="text">' +
                '<div class="icon"><img src="fragment/themes/inksumos/design/imgs/loader-01.svg"></div>' +
                '<div class="row1">Estamos enviando tu mensaje...</div>' +
                '<div class="row2"></div>' +
                '</div>' +
                '</div>';

            formEl.closest('.content').append(messageEl);
            this.setFormState("sending", formEl);
            this.setFormState("hidden", formEl);

        },

        /**
         * Remove form message.
         * @param {HTMLElement} form
         */
        removeFormMessage: function (form) {
            let formEl = form.element,
                formParent = $(formEl).parent(),
                messageEl = $(formEl).closest('.form-message'),
                msg = $('.block.contact .intro');
            let overlay = $('.contact-overlay');
            let body = $('body');
            setTimeout(() => {
                overlay.removeClass('sending');
                body.removeClass('no-scroll');
                this.setFormState("", formEl);
                $(formParent).find('.form-message').detach();
                this.setForm("", formEl);
                msg.fadeIn(10);
            }, 3500);
        },

        /**
         * Set form state.
         * @param {string} state
         * @param {HTMLElement} form
         */
        setFormState: function (state, form) {
            let regExp = new RegExp("\s?\\b(sending|error|success)\\b", "i");

            form.className = form.className.replace(regExp, "");
            form.className += ` ${ state }`;
        },

        setForm: function (state, form) {
            let regExp = new RegExp("\s?\\b(hidden)\\b", "i");

            form.className = form.className.replace(regExp, "");
            form.className += ` ${ state }`;
        }
    },
    /**
     * Init Profiles Form
     */
    initProfileForm: {
        init: function () {
            var form = document.querySelector('.woocommerce-EditAccountFor');
            if (typeof form === 'undefined' || form === null) {
                return;
            }
            let contact = document.querySelector('.woocommerce-EditAccountFor', this.elem);
            this.elem = document.querySelector('.woocommerce-MyAccount-content');
            let urlRoute = '/wp-admin/admin-ajax.php';
            let cf = new pl.ContactForm(contact, {
                url: urlRoute,
                action: 'upsert_usermetadata',
                inputSelectors: [
                    "input[type=text]",
                    "textarea"
                ]
            });

            cf.sending.add(() => {
                this.onSending(cf);
            });
            cf.error.add((status, statusText) => {
                this.onError(cf, status, statusText);
            });
            cf.success.add((response, status, statusText) => {
                this.onSuccess(cf, response, status, statusText);
            });
        },

        /**
         * Handle sending event.
         * @param {pl.ContactForm} form
         */
        onSending: function (form) {
            this.setFormState(form, 'sending');
        },

        /**
         * Handle sending event.
         * @param {pl.ContactForm} form
         */
        onError: function (form) {
            this.setFormState(form, 'error');
            this.removeFormState(form);
        },

        /**
         * Handle sending event.
         * @param {pl.ContactForm} form
         */
        onSuccess: function (form, response, status, statusText) {
            let res = parseInt(status);
            if (res) {
                this.setFormState(form, 'success');
                this.removeFormState(form);
            } else {
                this.onError(form, status, statusText);
            }
        },

        /**
         * Set form state.
         * @param form
         * @param state
         */
        setFormState: function (form, state) {
            let formEl = form.element;
            let serverRes = document.querySelector('.server-response');
            let serverResText = serverRes.querySelector('.server-response .text');

            pl.Classie.removeClass(formEl, 'sending');
            pl.Classie.removeClass(formEl, 'error');
            pl.Classie.removeClass(formEl, 'success');

            if (!!state.length) {
                switch (state) {
                    case 'sending':
                        pl.Classie.addClass(this.elem, 'sending');
                        break;
                    case 'error':
                        pl.Classie.removeClass(this.elem, 'sending');
                        pl.Classie.addClass(this.elem, 'error');
                        serverResText.innerHTML = `
                            <div>There was an error.</div>
                            <div>Please try again later.</div>
                        `;
                        break;
                    case 'success':
                        pl.Classie.removeClass(this.elem, 'sending');
                        pl.Classie.addClass(this.elem, 'success');
                        break;
                }

                pl.Classie.addClass(formEl, state);
            }
        },

        /**
         *
         */
        removeFormState: function (form) {
            let displayTime = 5; // Seconds

            setTimeout(() => {
                pl.Classie.removeClass(this.elem, 'error');
                pl.Classie.removeClass(this.elem, 'success');
                this.setFormState(form, '');
            }, displayTime * 1000);
        },
    },
    /**
     * Delete Profile
     */
    deleteProfileModal: {
        /**
         * @type Node
         */
        content: null,
        init: function () {

            let id = document.querySelector('.woocommerce-EditAccountFor #id');
            let btnModal = document.querySelector('button.delete');
            if (typeof id === 'undefined' || id === null && btnModal == 'undefined' || id === null) {
                return;
            }
            this.content = document.querySelector('.content.dummy-template');
            let dummyTemplate = document.querySelector('.dummy-template');
            let closeBtn = document.querySelector('button.cancelar');
            let acceptBtn = document.querySelector('button.aceptar');
            let modal = new pl.Modal({effectName: 'pl-effect-1'});
            let idVal = document.querySelector('.woocommerce-EditAccountFor #id').value;
            let btn = document.querySelector('.bottom-buttons a');

            // Create modal instance.
            btnModal.addEventListener('click', () => {
                modal.open(dummyTemplate);
            });
            closeBtn.addEventListener('click', () => {
                modal.close();
            });
            acceptBtn.addEventListener('click', function () {
                $.ajax({
                    type: "POST",
                    url: '/wp-admin/admin-ajax.php',
                    data: ({
                        action: "delete_usermetadata",
                        id: idVal
                    }),
                    error: function (response) {
                        Page.deleteProfileModal.toggleState('error');
                        setTimeout(function () {
                            btn.click();
                        }, 3000);
                    },
                    beforeSend: function (response) {
                        Page.deleteProfileModal.toggleState('sending');
                    },
                    success: function (response) {
                        Page.deleteProfileModal.toggleState('success');
                        setTimeout(function () {
                            btn.click();
                        }, 3000);
                    }
                })
            });
        },
        toggleState: function (state) {
            let options = this.content.querySelector('.options');
            pl.Classie.removeClass(options, 'sending');
            pl.Classie.removeClass(options, 'error');
            pl.Classie.removeClass(options, 'success');

            if (!!state.length) {
                switch (state) {
                    case 'sending':
                        pl.Classie.addClass(options, 'sending');
                        break;
                    case 'error':
                        pl.Classie.removeClass(options, 'sending');
                        pl.Classie.addClass(options, 'error');
                        break;
                    case 'success':
                        pl.Classie.removeClass(options, 'sending');
                        pl.Classie.addClass(options, 'success');
                        break;
                }
                pl.Classie.addClass(options, state);
            }
        }
    },
    /**
     * Contact
     */
    ContactInnerServices: {
        /**
         * @type jQuery
         */
        $elem: null,

        /**
         * Initialize page part.
         */
        init: function () {
            var contactForm = $(".block.contact-inner-services form");
            if (contactForm.length != 0) {

                // Initialize contact form.
                this.form = new pl.ContactForm(contactForm.get(0), {
                    //url: 'https://goplek.com/mailer/send-mail-v1.php',
                    url: 'wp-content/themes/inksumos/process-ajax.php',
                    inputSelectors: [
                        "input[type=text]",
                        "input[type=hidden]",
                        "textarea",
                    ],
                    useAjax: true,
                });

                this.onError = this.onError.bind(this);
                this.onSuccess = this.onSuccess.bind(this);
                this.onSending = this.onSending.bind(this);

                this.form.error.add(this.onError);
                this.form.success.add(this.onSuccess);
                this.form.sending.add(this.onSending);
                this.form.inputError.add(this.handleInputError.bind(this));
            }
        },
        /**
         * Fires if message fails.
         * @param {HTMLElement} form
         * @param {string} status
         * @param {string} statusText
         */
        onError: function (status, statusText, form) {
            var statusMessage = form.querySelector('.status-message');
            statusMessage.classList.remove('hidden');
            statusMessage.innerHTML = '<div class="error">Hubo un error al enviar tu mensaje, por favor intentalo más tarde</div>';
            this.removeFormStatus(form);
        },
        /**
         * Handle input error event.
         * @param {HTMLInputElement} input
         */
        handleInputError: function (input) {
            var clue = input.dataset['clue'];

            if (clue) {
                input.setAttribute('placeholder', clue);
            }
        },

        /**
         * Fires if message is sended successfully.
         * @param {HTMLElement} form
         * @param {any} response
         * @param {string} status
         * @param {string} statusText
         */
        onSuccess: function (response, status, statusText, form) {

            var statusMessage = form.querySelector('.status-message');
            statusMessage.classList.remove('d-none');
            statusMessage.innerHTML = '<div class="success"><div class="load-image-success"><img class="img-fluid" src="wp-content/themes/inksumos/assets/images/check-form.svg" alt=""></div>' +
                '<d class="message-success" >Mensaje recibido</div></div>';
            //Page.ga('Contacto', 'Enviado', 'Enviando');
            this.removeFormStatus(form);


        },
        /**
         * Fires when message is sending.
         * @param {HTMLElement} form
         */
        onSending: function (form) {
            var statusMessage = form.querySelector('.status-message');
            var elementHide = form.querySelector('.wrapper-content-form');
            elementHide.classList.add('partial-hide');
            statusMessage.classList.remove('d-none');
            statusMessage.innerHTML = '<div class="sending"><div class="load-image"><img class="img-fluid" src="wp-content/themes/inksumos/assets/images/drop-form.svg" alt=""></div>' +
                '<d class="message" >Enviando mensaje</div></div>';

        },
        /**
         * Remove Text
         */
        removeFormStatus: function (form) {
            var _this = this;
            var statusMessage = form.querySelector('.status-message');
            var elementHide = form.querySelector('.wrapper-content-form');
            var delay = 4000;

            setTimeout(function () {
                /**
                 * Get inputs and textarea to reset the placeholder
                 */
                _this.form.texts.forEach(function (input) {
                    input.setAttribute('placeholder', input.getAttribute('data-placeholder'));
                });

                elementHide.classList.remove('partial-hide');
                statusMessage.classList.add('d-none');

            }, delay);
        },
    },
    /**
     *
     */
    ShipmentAddress: {
        /**
         * @type NODE
         */
        container: null,
        /**
         * @type NODE
         */
        addressList: null,
        /**
         * @type NODE
         */
        target: null,
        /**
         * @type NODE
         */
        rfcInput: null,
        /**
         * @type NODE
         */
        rfcTarget: null,
        init: function () {
            let _this = this;
            this.container = document.querySelector('.shipping-wrapper');
            if (typeof this.container === 'undefined' || this.container === null) {
                return;
            }
            this.addressList = this.container.querySelectorAll('.address input');
            this.target = document.querySelector('.input-hidden');
            this.rfcList = document.querySelectorAll('.rfc-container input');
            this.rfcTarget = document.querySelector('#_shipping_rfc_field');

            /* Autocheck to force different shipping address */
            document.querySelector('#ship-to-different-address-checkbox').checked = true;
            /* Add Handlers to each radio button */
            for (var i = 0; i < this.addressList.length; i++) {
                this.addressList[i].addEventListener('click', function () {
                    _this.fillData(this);
                });
            }
            for (var i = 0; i < this.rfcList.length; i++) {
                this.rfcList[i].addEventListener('click', function () {
                    _this.fillRfcData(this);
                });
            }
        },

        fillData: function (data) {
            let custom_data = data.dataset;
            this.target.querySelector('#_shipping_rfc_name').value = custom_data['storename'];
            this.target.querySelector('#shipping_first_name').value = custom_data['name'];
            this.target.querySelector('#shipping_last_name').value = custom_data['lastName'];
            this.target.querySelector('#shipping_address_1').value = custom_data['address-1'];
            this.target.querySelector('#shipping_address_2').value = custom_data['address-2'];
            this.target.querySelector('#shipping_city').value = custom_data['city'];
            this.target.querySelector('#shipping_state').value = custom_data['state'];
            this.target.querySelector('#shipping_postcode').value = custom_data['postcode'];
        },

        fillRfcData: function (data) {
            this.rfcTarget.value = data.value;
        }
    },
    /**
     * Put the addresses elements into an Accordion
     */
    AddressAccordion: {
        /**
         * @type Node
         **/
        dashboard: null,
        /**
         * @type Node
         **/
        addressCont: null,
        /**
         * @type Node
         **/
        addressList: null,
        /**
         * @type Node
         **/
        arrow: null,
        /**
         * @type Node
         **/
        content: null,
        /**
         * @type Node
         **/
        container_address: null,
        /**
         * @type Node
         */
        height_address: null,
        /**
         * @type Number
         */
        total: null,
        /**
         * @type Number
         */
        size: null,
        /**
         * Initialice function
         */
        init: function () {
            this.dashboard = document.querySelector('.dashboard');
            if (typeof this.dashboard === 'undefined' || this.dashboard === null) {
                return;
            }
            this.addressCont = this.dashboard.querySelector('.addressess');
            this.addressList = this.dashboard.querySelectorAll('.addressess .content');
            this.arrow = this.dashboard.querySelector('.arrow');
            this.container_address = this.dashboard.querySelector('.container-address');
            this.height_address = this.dashboard.querySelector('.height-address');
            this.setSizes();
            this.arrow.addEventListener('click', () => {
                this.toogle();
            })
        },
        newHeight: function () {
            let targetHeight = this.height_address.offsetHeight;
            this.container_address.style.height = (targetHeight + this.size) + 'px';
        },
        setSizes: function () {
            if (this.addressList.length > 3) {
                for (let i = 0; i < 2; i++) {
                    this.size = $(this.addressList[i]).outerHeight(true);
                    this.total += $(this.addressList[i]).outerHeight(true);
                }
                this.container_address.style.height = this.total + 'px';

            }
            else {
                this.container_address.style.height = 'auto';
                pl.Classie.addClass(this.arrow, 'hidden');
            }
        },
        toogle: function () {
            if (!(pl.Classie.hasClass(this.addressCont, 'active'))) {
                pl.Classie.addClass(this.addressCont, 'active')
                this.newHeight();
            }
            else {
                pl.Classie.removeClass(this.addressCont, 'active');
                this.container_address.style.height = this.total + 'px';
            }
        }
    },
    /**
     *
     */
    FormFields: {
        init: function () {
            var checkbox = document.getElementById('billing-confirm');
            if (typeof checkbox === 'undefined' || checkbox === null) {
                return;
            }
            var billingContent = document.querySelector('.billing-wrapper'),
                billingFields = document.querySelector('.billing-fields');

            checkbox.addEventListener("change", validaCheckbox, false);

            function validaCheckbox() {
                var checked = checkbox.checked;
                if (checked) {
                    billingContent.style.display = "block";
                }
                else {
                    billingContent.style.display = "none";
                }
            }

            var check = document.querySelector('.different-address input');
            check.addEventListener('change', function (event) {
                if (event.target.checked) {
                    billingFields.style.display = "block";
                } else {
                    billingFields.style.display = "none";
                }
            });
        },
    },
    /**
     * Login Routes
     */
    LoginViews: {
        init: function () {
            var formLogin = document.querySelector('.woocommerce-form-login');
            var formSignUp = document.querySelector('.woocommerce-form-register');
            let logButton = document.querySelector('.log-in-btn a');
            let regButton = document.querySelector('.signup-btn a');
            if ((typeof formLogin === 'undefined' || formLogin === null) || (typeof formSignUp === 'undefined' || formSignUp === null)) {
                return;
            }
            var body = document.querySelector('body'),
                woocommerce = document.querySelector('.woocommerce'),
                pwd = document.querySelector('.woocommerce-LostPassword');

            if (window.location.hash !== '#register') {
                logButton.classList.add('active');
                body.classList.add('login-page');
                woocommerce.classList.add('register-wrapper');
                formSignUp.style.display = "none";
                woocommerce.style.background = "url(wp-content/plugins/woocommerce/assets/images/login-background.jpg)";
            }
            else {
                regButton.classList.add('active');
                body.classList.add('signup-page');
                woocommerce.classList.add('register-wrapper');
                formLogin.style.display = "none";
                woocommerce.style.background = "url(wp-content/plugins/woocommerce/assets/images/signup-background.jpg)";
                pwd.style.display = "none";
            }

            logButton.addEventListener('click', function () {
                window.location.hash = "#login";
                Page.LoginViews.checkHash();
            });
            regButton.addEventListener('click', function () {
                window.location.hash = "#register";
                Page.LoginViews.checkHash();
            })

        },
        checkHash: function () {
            if (window.location.hash === '#register' || window.location.hash === '#login') {
                location.reload();
            }
        },
    },
};
$(Page.init);
