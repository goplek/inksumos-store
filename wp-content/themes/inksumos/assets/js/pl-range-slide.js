// Reference: https://stackoverflow.com/questions/41068428/creating-a-pure-javascript-range-slider-control
(function (pl) {
    var RangeSlider = /** @class */ (function () {
        // endregion
        /**
         * Create range slider instance.
         * @param track
         * @param settings
         */
        function RangeSlider(track, settings) {
            // region Fields
            /**
             * Track element.
             * @type {HTMLElement}
             */
            this.track = null;
            /**
             * Dragging state.
             * @type {boolean}
             */
            this.dragging = false;
            /**
             * Current offset of the handle element.
             * @type {number}
             */
            this.handleOffset = 0;
            /**
             * Current offset.
             * @type {number}
             */
            this.offset = 0;
            /**
             * Handle element.
             * @type {HTMLElement}
             */
            this.handle = null;
            if (!track) {
                throw 'Can not create instance because param "el" is not valid.';
            }
            this.track = track;
            this.settings = settings;
            this.handle = document.createElement('div');
            this.handle.className += 'handle';
            this.track.appendChild(this.handle);
            this.calcSizes();
            this.initEvents();
        }
        // region Private methods
        /**
         * Calculate sizes.
         */
        RangeSlider.prototype.calcSizes = function () {
            this.handleWidth = this.handle.offsetWidth;
            var trackRect = this.track.getBoundingClientRect();
            this.trackWidth = trackRect.width;
            this.trackLeft = trackRect.left;
            this.trackRight = this.trackLeft + this.trackWidth;
            this.maxRight = this.trackWidth - this.handleWidth;
        };
        /**
         * Initialize events.
         */
        RangeSlider.prototype.initEvents = function () {
            this.handleMousedown = this.handleMousedown.bind(this);
            this.handleMousemove = this.handleMousemove.bind(this);
            this.handleMouseup = this.handleMouseup.bind(this);
            this.handleResize = this.handleResize.bind(this);
            // Desktop
            this.handle.addEventListener('mousedown', this.handleMousedown);
            window.addEventListener('mousemove', this.handleMousemove);
            window.addEventListener('mouseup', this.handleMouseup);
            // Mobile
            this.handle.addEventListener('touchstart', this.handleMousedown);
            window.addEventListener('touchmove', this.handleMousemove);
            window.addEventListener('touchend', this.handleMouseup);
            window.addEventListener('resize', this.handleResize);
        };
        /**
         * Handle mousedown event.
         * @param ev
         */
        RangeSlider.prototype.handleMousedown = function (ev) {
            var handleRect = this.handle.getBoundingClientRect();
            var clientX = ev.clientX || ev.touches[0].clientX;
            this.handleOffset = clientX - handleRect.left;
            this.dragging = true;
            this.handle.className += ' grabbing';
        };
        /**
         * Handle mousemove event.
         * @param ev
         */
        RangeSlider.prototype.handleMousemove = function (ev) {
            if (this.dragging) {
                var clientX = ev.clientX || ev.touches[0].clientX;
                this.offset = clientX - this.trackLeft - this.handleOffset;

                //console.log("%s > %s", this.offset, this.maxRight);

                if (this.offset < 0) {
                    this.offset = 0;
                } else if (this.offset > this.maxRight) {
                    this.offset = this.maxRight;
                }
                this.handle.style.left = this.offset + 'px';
                this.onUpdating();
            }
        };
        /**
         * Handle mouseup event.
         * @param ev
         */
        RangeSlider.prototype.handleMouseup = function (ev) {
            this.dragging = false;
            this.handle.className = this.handle.className.replace(/\s?grabbing/g, '');
        };
        /**
         * Handle resize event.
         * @param ev
         */
        RangeSlider.prototype.handleResize = function (ev) {
            this.calcSizes();
        };
        // endregion
        // region Methods
        /**
         * Gets current percentage of range slider.
         * @returns {number}
         */
        RangeSlider.prototype.getPercentage = function () {
            return (this.offset * 100) / this.maxRight;
        };
        /**
         * Gets current value of range slider.
         * @returns {number}
         */
        RangeSlider.prototype.getValue = function () {
            return (this.settings['maxValue'] || this.maxRight) * (this.getPercentage() / 100);
        };
        // endregion
        // region Events
        /**
         * Fires when range slider is updating.
         */
        RangeSlider.prototype.onUpdating = function () {
            if (this._updating) {
                var percentage = this.getPercentage();
                var value = this.getValue();
                this._updating.fire(percentage, value);
            }
        };
        Object.defineProperty(RangeSlider.prototype, "updating", {
            /**
             * Gets updating event.
             * @returns {pl.PLEvent}
             */
            get: function () {
                if (!this._updating) {
                    this._updating = new pl.PLEvent();
                }
                return this._updating;
            },
            enumerable: true,
            configurable: true
        });
        return RangeSlider;
    }());
    pl.RangeSlider = RangeSlider;
})(pl || (pl = {}));