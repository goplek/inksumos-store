<?php
/**
 * frequents ask
 * Template Name: Preguntas frecuentes
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

    <div class="outer-wrapper">

        <section class="block page faq">
            <div class="container">
                <div class="holder">
                    <?php
                    /* Start the Loop */
                    while (have_posts()) :
                        the_post();

                        the_content();

                    endwhile; // End of the loop.

                    ?>
                </div>
            </div>
        </section>
    </div>
<?php
get_footer();
