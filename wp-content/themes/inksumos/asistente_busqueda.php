<?php
/**
 * Template Name: Asistente de búsqueda
 *
 * Created by PhpStorm.
 * User: goplek18
 * Date: 5/28/19
 * Time: 1:44 PM
 */

get_header();
?>

<div class="outer-wrapper">

    <?php
    //Include section cover
    get_template_part('template-parts/section', 'cover-products');
    // Include section
    get_template_part('template-parts/section', 'assistant');
    // Include section
    get_template_part('template-parts/content/content', 'products-assistant');
    // Include Footer.
    get_footer();
    ?>
