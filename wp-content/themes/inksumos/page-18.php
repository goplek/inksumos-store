<?php
/**
 * The template for displaying all single posts
 *Template Name: Términos y condiciones
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<div class="outer-wrapper">

		<section class="block page">
			<div class="container">
				<div class="holder">
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						the_content();

					endwhile; // End of the loop.

					?>
				</div>
			</div>
		</section>
	</div>
<?php
get_footer();
