<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'db67487_inksumos' );
define('DB_NAME', 'inksumos');

/** MySQL database username */
//define( 'DB_USER', 'db67487_dev' );
define('DB_USER', 'root');

/** MySQL database password */
//define( 'DB_PASSWORD', 'D3v3l0p3r.' );
define('DB_PASSWORD', '');

/** MySQL hostname */
//define( 'DB_HOST', 'internal-db.s67487.gridserver.com' );
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'W/|a|ILN4A&&vZmNPp[EVF1ZYD1co5.J]Q$iu-~:hGQ;&`W|.gK+Kn5[Eu wNajK');
define('SECURE_AUTH_KEY', 'h<&+jVq8qU7<!0IP2#rR+]_70s]qqjMLl`|=6Zukcfp{,4uc;Cx{)$c#9?|P27CI');
define('LOGGED_IN_KEY', '9C!/Aq|u[4i;`!mM^%>BJ#*8;H|dvdjS,$A.=S:IeqStxS`HNK!J2=l/QZEQX=bz');
define('NONCE_KEY', '2K_`D>95Q@uO@.<M- l&&BN%6whtr~ZJ.G|4sjCAQd1xl,r=Z1KFu>efG+^#{]|p');
define('AUTH_SALT', '!?`:f2uE1MP{hl!y<W4Q/$r+XbEHjbnp|{TxW&jnr]+~dKve_fU=5AohdUZ5EgfZ');
define('SECURE_AUTH_SALT', '>+%x-X5oor|3vhJ_MH_99kYR@Oo3m+47RpyZ?NNlbI7~<^BE6xb;v&6lF]cZi`cG');
define('LOGGED_IN_SALT', 'm4f[HJ*.+>lfMY?@<ybb6;27y&Qjob:u%&9TISxrW&F+=J%h8CJdfzf_=w4HPCV^');
define('NONCE_SALT', ';xRU>Hp;#e*1h.3vr7beEIxfpn4oIgzD)SeqJ1XaoF5X9BNEST_7k~n@?r||T36E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
//define('WP_HOME', 'http://inksumos.goplek.com');
define('WP_HOME', 'http://inksumos.local');
//define('WP_SITEURL', 'http://inksumos.goplek.com');
define('WP_SITEURL', 'http://inksumos.local');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
